﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentScript : MonoBehaviour {

    [Tooltip("Inicialgoal es el punto hasta donde irá la cámara.")]
    public Transform inicialgoal;
    [Tooltip("Newgoal es el punto donde arranca el juego.")]
    public Transform newgoal;
    NavMeshAgent agent;
    // Use this for initialization
    private void Awake()
    {
        //Indicar el terreno hasta donde va la cámara
        //inicialgoal = null;
        newgoal = null;
    }

    void Start () {
        agent = GetComponent<NavMeshAgent>();
        agent.destination=inicialgoal.position;
    }
	
	// Update is called once per frame
	void Update () {

        //Cuando se acerque al final del recorrido, retrocede para iniciar la partida.
        print(agent.remainingDistance + " --->"+Time.time);
        if (agent.remainingDistance < 0.5f)
        {
            print("hohohoh");
            IniciarJuego();
        }

        //Atajos para movilizarse entre terrenos
        /*if (Input.GetKey(KeyCode.Alpha6))
        {
            newgoal = GameObject.FindGameObjectWithTag("GoalInsumos").GetComponent<Transform>();
            agent.SetDestination(newgoal.position);
            //ViewTienda();
        }
        if (Input.GetKey(KeyCode.Alpha7))
        {
            newgoal = GameObject.FindGameObjectWithTag("GoalEstanque").GetComponent<Transform>();
            agent.SetDestination(newgoal.position);
            //ViewEstanque();
        }
        if (Input.GetKey(KeyCode.Alpha8))
        {
            newgoal = GameObject.FindGameObjectWithTag("GoalMercado").GetComponent<Transform>();
            agent.SetDestination(newgoal.position);
            //ViewMercado();
        }
        if (Input.GetKey(KeyCode.Alpha9))
        {
            newgoal = GameObject.FindGameObjectWithTag("GoalGremio").GetComponent<Transform>();
            agent.SetDestination(newgoal.position);
            //ViewMercado();
        }*/

    }

    //Función utilizada para que retorne al estanque
    void IniciarJuego()
    {
        newgoal = GameObject.FindGameObjectWithTag("GoalEstanque").GetComponent<Transform>();
        agent.SetDestination(newgoal.position);
    }

    }
