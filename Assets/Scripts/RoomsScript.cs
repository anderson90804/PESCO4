using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;
using UnityEngine.Networking;
using System;
using System.Collections;

public class GameCard
{
    private GameObject card;
    private Image img;
    private Text title;
    private Text description;
    private string map;

    public GameCard(GameObject card, Action<string> loadMap)
    {
        this.card = card;
        this.img = card.transform.Find("Preview").gameObject.GetComponent<Image>();
        this.title = card.transform.Find("Titulo/Texto").gameObject.GetComponent<Text>();
        this.description = card.transform.Find("Descripción/Texto").gameObject.GetComponent<Text>();
        this.card.transform.Find("Titulo")
            .gameObject.GetComponent<Button>()
            .onClick.AddListener(() => { loadMap(this.map); });
        this.card.transform.Find("Preview/PlayBtn")
            .gameObject.GetComponent<Button>()
            .onClick.AddListener(() => { loadMap(this.map); });
    }

    public void setTitle(string title)
    {
        this.title.text = title;
    }

    public void setImgSprite(Sprite sprite)
    {
        this.img.sprite = sprite;
    }

    public void setDescription(string description)
    {
        this.description.text = description;
    }

    public void setMap(string map)
    {
        this.map = map;
    }

}

public class RoomsScript : MonoBehaviour
{
    public GameObject[] roomCards;
    public GameCard[] gameCards;
    public RectTransform[] pages;
    public Button previous;
    public Button next;
    public ScrollRect Levelscroll;
    public float speedAnimScroll = 0.7f;

    [Header("PANELS")]
    public GameObject panelMenuLevels;
    public GameObject panelLoading;


    private int page = 1;//pagina actual de niveles
    public float positionScroll
    {
        get => (1.0f / (pages.Length - 1)) * (page - 1);
    }//posicion actual del scroll

    public void Awake()
    {
        // We create a new GameCard for each roomCard
        gameCards = new GameCard[roomCards.Length];
        for (int i = 0; i < roomCards.Length; i++)
        {
            gameCards[i] = new GameCard(
                roomCards[i],
                loadLevel
            );
        }
    }

    public async void Start()
    {
        RoomInfo[] rooms = RoomService.getRooms();

        int i = 0;
        foreach (var card in gameCards)
        {
            Debug.Log(rooms[i].imageUrl);
            setSpriteCard(rooms[i].imageUrl, card);
            card.setTitle(rooms[i].name);
            card.setDescription(rooms[i].description);
            card.setMap(rooms[i].map);
            i++;
        }
    }
    public async Task setSpriteCard(string url, GameCard card)
    {
        card.setImgSprite(
                Sprite.Create(
                    await GetTextureFromImageURL(url), // Async wait for the texture
                    new Rect(0, 0, 800, 480), // A rect that defines the area of the texture to use
                    new Vector2(0.5f, 0.5f) // A pivot point for the texture
                )
            );
    }

    public async Task<Texture2D> GetTextureFromImageURL(string url)
    {
        using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url))
        {
            www.SendWebRequest();

            while (!www.isDone)
            {
                await Task.Delay(100);
            }

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
                return null;
            }
            else
            {
                // Get downloaded asset bundle
                return DownloadHandlerTexture.GetContent(www);
            }
        }
    }

    private void loadLevel(string map)
    {
        panelMenuLevels.SetActive(false);
        panelLoading.SetActive(true);
        AsyncOperation levelOperation = SceneManager.LoadSceneAsync(map, LoadSceneMode.Additive);
        levelOperation.completed += (operation) => { SceneManager.UnloadSceneAsync("levelMenu"); };
    }

    public void Update()
    {
        // ...
    }

    public void regresarMenu()
    {
        SceneManager.LoadScene("startMenu");
    }

    public void nextPage()
    {
        if (page < pages.Length)
        {
            page++;
            StopCoroutine(moveScroll(false));
            StartCoroutine(moveScroll(true));
        }
        previous.interactable = page > 1 ? true : false;
        next.interactable = page < pages.Length ? true : false;
    }

    public void previousPage(){
        if(page > 1) {
            page--;
            StopCoroutine(moveScroll(true));
            StartCoroutine(moveScroll(false));
        }
        previous.interactable = page > 1 ? true : false;
        next.interactable = page < pages.Length ? true : false;
    }

    //dir positivo para moverse a la siguiente pagina y negativo para regresarse
    IEnumerator moveScroll(bool dir)
    {
        while ((Levelscroll.horizontalNormalizedPosition < positionScroll && dir) || 
            (Levelscroll.horizontalNormalizedPosition > positionScroll && !dir))
        {
            Levelscroll.horizontalNormalizedPosition += Time.deltaTime * this.speedAnimScroll * (dir ? 1 : -1);
            yield return null;
        }
    }
}