using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class creditoScrollView : MonoBehaviour
{
    public ScrollRect scrollRect;
    public float velocity = 0.05f;
    // Start is called before the first frame update
    private void OnEnable()
    {
        StartCoroutine(moveScroll());
    }

    // Update is called once per frame
    IEnumerator moveScroll()
    {
        yield return new WaitForSeconds(1);
        while (scrollRect.verticalNormalizedPosition > 0)
        {
            scrollRect.verticalNormalizedPosition -= Time.deltaTime * velocity;
            yield return null;
        }
    }

    private void OnDisable()
    {
        scrollRect.verticalNormalizedPosition = 1.0f;
    }
}
