using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class JuegoLibreDesc : MonoBehaviour
{
    public ScrollRect scrollRect;
    public Button previous;
    public Button next;
    public float velocity = 0.05f;
    public int page = 1;
    public float positionScroll
    {
        get => (1.0f / 2.0f) * (page - 1);
    }//posicion actual del scroll
    // Start is called before the first frame update
    private void Start()
    {
        scrollRect.horizontalNormalizedPosition = 0;
    }

    public void nextPage()
    {
        if (page < 3)   
        {
            page++;
            scrollRect.horizontalNormalizedPosition = positionScroll;
        }
        previous.interactable = page > 1 ? true : false;
        next.interactable = page < 3 ? true : false;
    }

    // Update is called once per frame
    public void backPage()
    {
        if (page > 1)
        {
            page--;
            scrollRect.horizontalNormalizedPosition = positionScroll;
        }
        previous.interactable = page > 1 ? true : false;
        next.interactable = page < 3 ? true : false;
    }

    public void regresarMenu()
    {
        SceneManager.LoadScene("startMenu");
    }

    public void PlayFreeGame()
    {
        SceneManager.LoadScene("JuegoLibre");
    }
}
