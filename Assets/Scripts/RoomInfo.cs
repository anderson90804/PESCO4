public class RoomInfo{
    // id of the room
    public string id;

    // map id this refers to a map in the map table (levels)
    public string map;
    
    // this is the card info
    public string name;
    public string description;
    public string imageUrl;

    public RoomInfo(){
        this.id = "";
        this.map = "";
        this.name = "";
        this.description = "";
        this.imageUrl = "";
    }

    public RoomInfo(string id, string map, string name, string description, string imageUrl){
        this.id = id;
        this.map = map;
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public string getId(){
        return this.id;
    }

    public string getMap(){
        return this.map;
    }

    public string getName(){
        return this.name;
    }

    public string getDescription(){
        return this.description;
    }

    public string getImageUrl(){
        return this.imageUrl;
    }

    public void setId(string id){
        this.id = id;
    }

    public void setMap(string map){
        this.map = map;
    }

    public void setName(string name){
        this.name = name;
    }

    public void setDescription(string description){
        this.description = description;
    }

    public void setImageUrl(string imageUrl){
        this.imageUrl = imageUrl;
    }
}