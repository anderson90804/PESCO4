using UnityEngine;

public class AuthRequestDto{
    private string username;
    private string password;

    public AuthRequestDto(){
        this.username = "";
        this.password = "";
    }

    public AuthRequestDto(string token, string username, string password){
        this.username = username;
        this.password = password;
    }

    public string getUsername(){
        return this.username;
    }

    public string getPassword(){
        return this.password;
    }

    public void setUsername(string username){
        this.username = username;
    }

    public void setPassword(string password){
        this.password = password;
    }
}