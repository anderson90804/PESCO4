public class RoomService {
    public static RoomInfo[] getRooms(){
        RoomInfo[] rooms = new RoomInfo[7];
        rooms[0] = new RoomInfo("1", "Nivel1", "Nivel 1", "Producci�n de un solo pez con sistema automatizado.\n\nLa primera decisi�n es la venta del pez.", "/imagenes/pez-payaso.jpg");
        rooms[1] = new RoomInfo("2", "Nivel2", "Nivel 2", "Producci�n de un solo pez con raci�n variable.\n\nNueva decisi�n: Suministro de racion\n\n a partir de este nivel se permite indicar la racion de alimento para el pez.", "/imagenes/pez-payaso.jpg");
        rooms[2] = new RoomInfo("3", "Nivel3", "Nivel 3", "Producci�n de lote de peces con sistema de producci�n automatizado.\n\nNueva decisi�n: Compra de lote de peces\nA partir de este nivel se permite comprar un lote de peces.", "/imagenes/pez-payaso.jpg");
        rooms[3] = new RoomInfo("4", "Nivel4", "Nivel 4", "Producci�n de lote de peces con raci�n variable.\n\nNueva decisi�n: Compra de alimento\nA partir de este nivel se explica la relaci�n del alimento con el crecimiento del lote de peces.", " /imagenes/pez-payaso.jpg");
        rooms[4] = new RoomInfo("5", "Nivel5", "Nivel 5", "Producci�n de lote de peces con raci�n y temperarura variable.\n\nNueva decisi�n: Alimentaci�n en funci�n de la temperatura\nA partir de este nivel se permite la temperatura de la simulaci�n que var�a por cada d�a de producci�n.", " /imagenes/pez-payaso.jpg");
        rooms[5] = new RoomInfo("6", "Nivel6", "Nivel 6", "Operaci�n en el mercado en modalidad asociativa.\n\nNueva decisi�n: Venta por turno\nA partir de este nivel se permite la venta por turnos en el mercado.", "/imagenes/pez-payaso.jpg");
        rooms[6] = new RoomInfo("7", "Nivel7", "Nivel 7", "Operaci�n en el mercado en modalidad de demanda fija.\n\nNueva decisi�n: Venta por gremio\nA partir de este nivel se permite la venta por demanda fija con el gremio.", " / imagenes/pez-payaso.jpg");
        return rooms;
    }

    public static RoomInfo getLevelInfo(string room){
        RoomInfo roomInfo = new RoomInfo("1", "map1", "name1", "description1", "imageUrl1");
        return roomInfo;
    }
}