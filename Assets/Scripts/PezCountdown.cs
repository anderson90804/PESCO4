﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
//using UnityEngine.UI;

public class PezCountdown : MonoBehaviour {
    //Variables de duración
    public GameObject player;
    public GameObject muro;
    public AudioSource audio0;
    public AudioSource audio1;
    public AudioSource audio2;
    public bool timeover;
    //public AudioSource audio11;
    //public AudioSource audio12;
    public int timeleft;
    public string mensaje;



    // Use this for initialization
    void Start()
    {
        AudioSource[] allAudioSources = GetComponents<AudioSource>();
        audio1 = allAudioSources[0];
        audio2 = allAudioSources[1];
        audio0 = allAudioSources[2];
        timeover = false;
        /*timeleft = 5;
        mensaje = "";*/
        StartCoroutine("Playtimer");

    }


    private IEnumerator Playtimer()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timeleft -= 1;
            audio0.Play();
            if (timeleft == 3 || timeleft==2 || timeleft==1)
            {
                audio0.Stop();
                audio1.Play();  
            }
                     
        }
    }

    private void OnGUI()
    {
        GUI.color = Color.black;
        GUI.Label(new Rect(50, 10, 400, 50), "Tiempo restante:" + timeleft.ToString());
        GUI.Label(new Rect(50, 30, 400, 50), mensaje);
    }

    void Update()
    {
        if (timeover==false)
        {
            audio2.Stop();
            if (timeleft == 0)
            {
                Debug.Log("Acabó el tiempo");
                audio0.Stop();
                audio1.Stop();
                mensaje = "Ya debió tomar la decisión";
                StopAllCoroutines();
                audio2.Play();
                timeleft=0;
                MeshRenderer m =muro.GetComponent<MeshRenderer>();
                m.enabled = false;
                timeover=true;
            }
        }else
        {
            //Accione el pez
            //player.GetComponent<PezPlayerController>().enabled = false;
        }
        
    }
}
