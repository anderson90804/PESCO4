﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidScript : MonoBehaviour {

    public LotePecesController controlador;
    public Vector3 distanceToCenter;
    public float speed = 0.5f;
    [SerializeField]
    float rotationSpeed = 4.0f;
    [SerializeField]
    Vector3 averageHeading;
    [SerializeField]
    Vector3 averagePosition;
    [SerializeField]
    float neighbourDistance = 0.05f;

    public bool turning = false;
	// Use this for initialization
	void Start () {
        speed = Random.Range(0.5f, 1);
        controlador = GameObject.FindObjectOfType<LotePecesController>();
	}
	
	// Update is called once per frame
	void Update () {
        float distX = transform.position.x - controlador.tankCenter.x;
        float distY = transform.position.y - controlador.tankCenter.y;
        float distZ = transform.position.z - controlador.tankCenter.z;
        distanceToCenter = new Vector3(Mathf.Abs(distX), Mathf.Abs(distY), Mathf.Abs(distZ));
        if((distanceToCenter.x>=controlador.tankSizeX) || (distanceToCenter.y >= controlador.tankSizeY) || (distanceToCenter.z >= controlador.tankSizeZ))
        {
            turning = true;
            //print(gameObject.name + "-->" + distanceToCenter);
            //print(gameObject.name+"-->"+ Vector3.Distance(transform.position, controlador.tankCenter)+Time.time);
        }
        else
        {
            turning = false;
        }

        if (turning)
        {
            Vector3 direction = controlador.tankCenter - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
            speed = Random.Range(0.5f, 1);
        }
        else
        {
            if (Random.Range(0, 5) < 1)
                ApplyRules();
        }

        transform.Translate(0, 0, Time.deltaTime * speed);
	}

    void ApplyRules()
    {
        GameObject[] gos;
        gos = controlador.peces;

        Vector3 vcentre = Vector3.zero;
        Vector3 vavoid = Vector3.zero;
        float gSpeed = 0.1f;

        Vector3 goalPos = controlador.goalPos;

        float dist;

        int groupSize = 0;
        foreach(GameObject go in gos)
        {
            dist = Vector3.Distance(go.transform.position, this.transform.position);
            if (dist <= neighbourDistance)
            {
                vcentre += go.transform.position;
                groupSize++;

                if (dist < 0.05f)
                {
                    vavoid = vavoid + (this.transform.position - go.transform.position);
                }

                BoidScript anotherBoid = go.GetComponent<BoidScript>();
                gSpeed = gSpeed + anotherBoid.speed;
            }

            if(groupSize>0)
            {
                vcentre = vcentre / groupSize + (goalPos - this.transform.position);
                speed = gSpeed / groupSize;

                Vector3 direction = (vcentre + vavoid) - transform.position;
                if (direction != Vector3.zero)
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
            }
        }
    }
}
