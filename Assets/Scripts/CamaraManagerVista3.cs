﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamaraManagerVista3 : MonoBehaviour {

    public Camera MainCamera;
    public Camera InsumosCamera;
    public Camera FishCamera;
    public Camera MercadoCamera;
    public Camera GremioCamera;
	
	// Atajos para ver las cámaras
	void Update () {
        if (Input.GetKey(KeyCode.Q))
        {
            ViewWorld();
        }
        else if (Input.GetKey(KeyCode.W))
        {
            ViewEstanque();
        }
        else if (Input.GetKey(KeyCode.E))
        {
            ViewInsumos();
        }
        else if (Input.GetKey(KeyCode.R))
        {
            ViewMercado();
        }
        else if (Input.GetKey(KeyCode.T))
        {
            ViewGremio();
        }
    }

    void OnTriggerEnter()
    {

        MainCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(true);
        MercadoCamera.gameObject.SetActive(false);
    }

    void OnTriggerExit()
    {
        MainCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(false);
        MercadoCamera.gameObject.SetActive(false);

    }

    public void ViewWorld()
    {
        //Ver la Camara Main
        MainCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(false);
        MercadoCamera.gameObject.SetActive(false);
    }

    public void ViewInsumos()
    {
        //Ver la Camara de Insumos
        MainCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(false);
        InsumosCamera.gameObject.SetActive(true);
        MercadoCamera.gameObject.SetActive(false);
        GremioCamera.gameObject.SetActive(false);
    }

    public void ViewEstanque()
    {
        //Ver la Camara del Estanque
        MainCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(true);
        MercadoCamera.gameObject.SetActive(false);
    }

    public void ViewMercado()
    {
        //Ver la Camara del Mercado
        MainCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(false);
        MercadoCamera.gameObject.SetActive(true);
    }

    public void ViewGremio()
    {
        //Ver la Camara del Gremio
        MainCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(false);
        InsumosCamera.gameObject.SetActive(false);
        MercadoCamera.gameObject.SetActive(false);
        GremioCamera.gameObject.SetActive(true);
    }

}
