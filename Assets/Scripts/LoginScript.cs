// login form in unity using input field and button in unity 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoginScript : MonoBehaviour
{
    public InputField username;
    public InputField password;
    public Button loginButton;

    // Start is called before the first frame update
    void Start()
    {
        loginButton.onClick.AddListener(login);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void login()
    {
        SceneManager.LoadScene("startMenu");
        // string user = username.text;
        // string pass = password.text;

        // check if is development build or not
        //if (Application.isEditor )
        //{
            // if(user == "admin" && pass == "admin"){
            //     SceneManager.LoadScene("startMenu");
            //     //Debug.Log(AuthPlugin.login(user, pass));
            // }
            // else
            // {
            //     Debug.Log("Wrong username or password");
            // }
        //}

        // we use a web api to check if the user is valid or not
        //else {
            // check if the user is valid or not
            // if valid then load the startMenu scene
            // else show error messag
           // Debug.Log(AuthPlugin.login(user, pass));
        //}
    }
}