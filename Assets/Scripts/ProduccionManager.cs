﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ProduccionManager : MonoBehaviour {
    
    Dictionary<string, Dictionary<string, int>> datosProduccion;
    int contadorCambios = 0;

	// Use this for initialization
	void Start () {
		
	}

    private void Init()
    {
        if (datosProduccion != null)
                return;
            datosProduccion = new Dictionary<string, Dictionary<string, int>>();
    }

    public void Reset()
    {
        contadorCambios++;
        datosProduccion = null;
    }

    public int GetDato(string dia, string tipoDato)
    {
        Init();
        if(datosProduccion.ContainsKey(dia)==false)
        {
            return 0;
        }
        return datosProduccion[dia][tipoDato];
    }
    
    public void SetDato(string dia, string tipoDato, int value)
    {
        Init();
        contadorCambios++;
        if(datosProduccion.ContainsKey(dia)==false)
        {
            datosProduccion[dia] = new Dictionary<string, int>();
        }
        datosProduccion[dia][tipoDato] = value;    
    }

    public void cambiarDato(string dia, string tipoDato, int monto)
    {
        Init();
        int datoActual = GetDato(dia, tipoDato);
        SetDato(dia, tipoDato, datoActual + monto);
    }

    /*
    public string[] GetInformacion()
    {
        Init();
        return datosProduccion.Keys.ToArray();
    }
    */
    public string[] GetInformacion(string sortingScoreType)
    {
        Init();

        return datosProduccion.Keys.OrderByDescending(n => GetDato(n, sortingScoreType)).ToArray();
    }

    public int GetContadorCambios()
    {
        return contadorCambios;
    }

    public void IniciarLista()
    {
        SetDato("0", "peso", 0);
    }

    public void AgregarInfoDia(string dia, int valor)
    {
        cambiarDato(dia, "peso", valor);
    }


    // Update is called once per frame
    void Update () {
		
	}
}
