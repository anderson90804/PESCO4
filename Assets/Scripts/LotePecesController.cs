﻿using System.Collections;

using UnityEngine;
using System;

public class LotePecesController : MonoBehaviour {

    public GameObject pezPrefab;
    public GameObject goalPrefab;
    [SerializeField]
    private int pezCount;
    [SerializeField]
    private int pezPeso;

    public float tankSizeX;
    public float tankSizeY;
    public float tankSizeZ;
    public Vector3 tankCenter;
    public Vector3 goalPos = Vector3.zero;

    [SerializeField]
    public GameObject[] peces;
    public GameObject goal;

    public int maxXNum = 2;
    public int maxYNum = 3;
    public int maxZNum = 4;
    private float MAXSCALE;
    private int proximoPesoCambio;
    public Vector3 meanPos; 
    public float rotateSpeed = 1f;
    public GameObject meanDummy;

    private void Awake()
    {
        this.MAXSCALE = 2f;
        this.tankCenter = gameObject.transform.position;
        goal=Instantiate(goalPrefab, tankCenter, Quaternion.identity);
        goal.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }

    void Start()
    {   
        //crearPeces();
    }

    void Update()
    {
        if (peces.Length!=0)
        {
            if (UnityEngine.Random.Range(0,10000)<50)
            {
                goalPos=new Vector3(UnityEngine.Random.Range(this.tankCenter.x - (float)tankSizeX, this.tankCenter.x + (float)tankSizeX), UnityEngine.Random.Range(this.tankCenter.y - (float)tankSizeY, this.tankCenter.y + (float)tankSizeY), UnityEngine.Random.Range(this.tankCenter.z - (float)tankSizeZ, this.tankCenter.z + (float)tankSizeZ));
                goal.transform.position = goalPos;
                //print("Cambio de goal pos" + Time.time);
            }
        }
        else
        {
            //Se acabaron los peces
            Array.Resize(ref peces, this.pezCount);
            goal.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    private void CrearPeces()
    {
        this.tankCenter = gameObject.transform.position;
        //Método para instanciar
        peces = new GameObject[this.pezCount];
        for (int i = 0; i < this.pezCount; i++)
        {
            Vector3 pos = new Vector3(UnityEngine.Random.Range(this.tankCenter.x - (float)tankSizeX, this.tankCenter.x + (float)tankSizeX), UnityEngine.Random.Range(this.tankCenter.y - (float)tankSizeY, this.tankCenter.y + (float)tankSizeY), UnityEngine.Random.Range(this.tankCenter.z - (float)tankSizeZ, this.tankCenter.z + (float)tankSizeZ));
            peces[i] = (GameObject)Instantiate(pezPrefab, pos, Quaternion.identity);
            peces[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            print(pos);
        }
    }
    
    public GameObject[] GetArregloPeces()
    {
        return this.peces;
    }
    public float GetMaxScale()
    {
        return this.MAXSCALE;
    }

    public int GetPezCount()
    {
        return this.pezCount;
    }

    public Vector3 GetGoalPos()
    {
        return this.goalPos;
    }

    public Vector3 GetTankCenter()
    {
        return this.tankCenter;
    }

    public void SetPezPeso(double peso)
    {
        this.pezPeso = (int)peso;
        CrecerPeces();
    }

    public void SetPezCount(int pecesEnLote)
    {
        int diferenciaPoblacion =this.pezCount-pecesEnLote;
        if (diferenciaPoblacion<0)
        {
            print("Se crearon peces" + Time.time+ " Diferencia población es:"+diferenciaPoblacion);
            print("El controlador tiene en total:"+this.pezCount+ " --  Los peces del jugador son"+pecesEnLote);
            this.pezCount = pecesEnLote;
            CrearPeces();
        }
        this.pezCount = pecesEnLote;
        MorirPeces(diferenciaPoblacion);
        Array.Resize(ref peces, pezCount);
    }

    //Método para destruir Gameobjects(de tipo pez, ya sea porque se venden o porque se mueren)
    public void MorirPeces(int pecesMuertos)
    {
        for (int i = 0; i < pecesMuertos; i++)
        {
            GameObject.Destroy(peces[(this.pezCount + i)]);
        }
    }

    //Método para adaptar el tamaño del arreglo que contiene los GameObjects de peces y adaptarlos a la cantidad de peces vivos en determinado momento
    private static Array ResizeArray(Array arr, int[] newSizes)
    {
        if (newSizes.Length != arr.Rank)
            throw new ArgumentException("arr must have the same number of dimensions " +
                                        "as there are elements in newSizes", "newSizes");

        var temp = Array.CreateInstance(arr.GetType().GetElementType(), newSizes);
        int length = arr.Length <= temp.Length ? arr.Length : temp.Length;
        Array.ConstrainedCopy(arr, 0, temp, 0, length);
        return temp;
    }

    private void CrecerPeces()
    {
        if (pezPeso > proximoPesoCambio)
        { 
            float escala = 0.5f;
            escala = (float)CalcularProximaEscala(pezPeso);
            Vector3 crecimiento = new Vector3(escala, escala, escala);
            //Cambio de escala de peces
            for (int i = 0; i < pezCount; i++)
            {
                peces[i].transform.localScale = crecimiento;
            }
        }

    }

    private float CalcularProximaEscala(int pesoActual)
    {
        float escalaActual = 0.5f;
        if (pesoActual < 5)
        {
            proximoPesoCambio = 5;
            escalaActual = 0.505f;
        }
        if (pesoActual < 10)
        {
            proximoPesoCambio = 10;
            escalaActual = 0.51f;
        }
        if (pesoActual < 20)
        {
            proximoPesoCambio = 20;
            escalaActual = 0.52f;
        }
        if (pesoActual < 30)
        {
            proximoPesoCambio = 30;
            escalaActual = 0.53f;
        }
        if (pesoActual < 40)
        {
            proximoPesoCambio = 40;
            escalaActual = 0.54f;
        }
        if (pesoActual < 50)
        {
            proximoPesoCambio = 50;
            escalaActual = 0.55f;
        }
        else if (pesoActual < 100)
        {
            proximoPesoCambio = 100;
            escalaActual = 0.6f;
        }
        else if (pesoActual < 150)
        {
            proximoPesoCambio = 150;
            escalaActual = 0.7f;
        }
        else if (pesoActual < 200)
        {
            proximoPesoCambio = 200;
            escalaActual = 0.8f;
        }
        else if (pesoActual < 250)
        {
            proximoPesoCambio = 250;
            escalaActual = 0.9f;
        }
        else if (pesoActual < 300)
        {
            proximoPesoCambio = 300;
            escalaActual = 1f;
        }
        else if (pesoActual < 350)
        {
            proximoPesoCambio = 350;
            escalaActual = 1.2f;
        }
        else if (pesoActual < 400)
        {
            proximoPesoCambio = 400;
            escalaActual = 1.4f;
        }
        else if (pesoActual < 450)
        {
            proximoPesoCambio = 450;
            escalaActual = 1.6f;
        }
        else if (pesoActual < 500)
        {
            proximoPesoCambio = 500;
            escalaActual = 1.8f;
        }
        else
        {
            escalaActual = MAXSCALE;
        }

        return escalaActual;
    }
}
