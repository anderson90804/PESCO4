﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class PlayerControllerCapsule : MonoBehaviour
{
    public float velMovimiento;
    public float velRotacion;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * velRotacion;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * velMovimiento;

        //Vector3 movement = new Vector3(x, 0.0f, 0.0f);
        //rb.AddForce(movement * speed);

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
    }

}