﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewCamGremio : MonoBehaviour
{

    public GameObject vistaGremio;
    // Use this for initialization
    void Start()
    {
        //vistaMundo = GameObject.FindGameObjectWithTag("Main Camera");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            vistaGremio.gameObject.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        vistaGremio.gameObject.SetActive(false);
    }
    
}
