﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.EventSystems;

public class UIControllerN4 : MonoBehaviour {

    /*En este script se colocan todos los controles del UI en función de los estados de juego y de las acciones*/

    //Modelo vigente y datos del juego
    public GameLogicN4 datosjuego;
    public ModeloN4 modelo;
    public Text nombreJugador;
    public Text nombreSesion;
    public Camera camaraEstanque;
    public Canvas canvasInsumos;
    public Text HUDNivel;
    public Image HUDLogoPesco;
    public Camera camaraMercado;
    public Canvas canvasMercado;
    public Camera camaraResumen;
    public Canvas canvasResumen;
    //Campos que cambian del HUD
    public Text HUDDia;
    public Text HUDDinero;
    public Text HUDPeces;
    public Text HUDAlimento;
    //Campos que cambian de la UI
    //Campos que cambian de los paneles de cada escenario del mapa
    //Panel de Insumos
    public Text PIMensaje;
    public Text PIPrecioPez;
    public Text PIPrecioAlimento;
    public Text PIInventarioAlimento;
    //Panel del Estanque
    public Text PEPeso;
    public Text PEConsumoActual;
    public Text PEFactorConversion;
    public Text PEBiomasa;
    public Text PECostoKg;
    //PE - Pestaña Ración
    public Text PERacionRequerida;
    public Text PERacionASuministrar;
    public Text PERacionSuministrada;
    public Text PERacionLote;
    //PE - Pestaña Estanque
    public Text PEAreaEstanque;
    public Text PENivelOxigeno;
    public Text PEDensidadGramosPeces;
    public Text PEPecesMuertos;
    //PE - Pestaña Costos
    public Text PECostoPeces;
    public Text PECostoMantenimiento;
    public Text PECostoAlimentacion;
    public Text PETotalCostos;
   
    //Panel del Mercado
    public Text PMMensaje;
    public Text PMCostosTotales;
    public Text PMBiomasa;
    public Text PMCostoKg;
    public Text PMPrecioKg;
    //Resumen produccion
    public Text RPPecesVendidos;
    public Text RPVentas;
    public Text RPPecesIniciales;
    public Text RPCostoPeces;
    public Text RPCostoMantenimiento;
    public Text RPCostoAlimento;
    public Text RPTotalCostos;
    public Text RPGanancia;
    private string vacio = "...";
    //Botones que cambian del juego
    public Button btnUIComprarPeces;
    public Button btnUIComprarAlimento;
    public Button btnCambiarRacion;
    public Button btnUIVenderPeces;
    //Mensajes de error y alerta
    private bool control;
    private bool alertaAlimentoActivada;
    private bool controlAnimacionProduccion;
    private bool controlAnimacionVenta;
    public Image imgErrorDineroInsuficiente;
    public Image imgErrorLimiteLote;
    public Image imgErrorPecesInsuficientes;
    public Image imgErrorAlimentoInsuficiente;
    public Image imgErrorPesoMinimo;
    public Image imgErrorConexion;
    public Image imgResumenProduccion;
    public Image imgGuardarEstado;
    //Módulos
    public Image imgModuloCompraPeces;
    public Image imgModuloVentaPeces;
    public Image imgModuloInfoVenta;
    //Módulo de duración del día
    public Text SDDValorDia;


    // Use this for initialization
    void Start()
    {
        alertaAlimentoActivada = false;
        control = true;
        controlAnimacionProduccion = false;
        controlAnimacionVenta = false;
        modelo.GetModeloActual();
        StartCoroutine(CambioColor(HUDDinero, true));
    }

    // Update is called once per frame
    void Update()
    {
        alertaAlimentoActivada = datosjuego.GetAlertaAlimento();
        IniciarAlerta(alertaAlimentoActivada);
        ReproducirAnimacionInicioProduccion(datosjuego.GetEstadoProduccion());
        if (datosjuego.GetEstadoProduccion())
        {
            //HUDLogoPesco.gameObject.transform.Rotate(new Vector3(0, 0, -90) * Time.deltaTime);
        }
        //Restricciones en caso de estar produciendo
        if (datosjuego.GetEstadoProduccion())
        {
            PIMensaje.gameObject.SetActive(true);
            imgModuloCompraPeces.gameObject.SetActive(false);
            PMMensaje.gameObject.SetActive(false);
            imgModuloVentaPeces.gameObject.SetActive(true);
            imgModuloInfoVenta.gameObject.SetActive(true);
            btnUIComprarPeces.interactable = false;
            btnUIVenderPeces.interactable = true;
        }
        else
        {
            PIMensaje.gameObject.SetActive(false);
            imgModuloVentaPeces.gameObject.SetActive(false);
            PMMensaje.gameObject.SetActive(true);
            imgModuloCompraPeces.gameObject.SetActive(true);
            imgModuloInfoVenta.gameObject.SetActive(false);
            btnUIComprarPeces.interactable = true;
            btnUIVenderPeces.interactable = false;
        }
        //Validación de entradas (Inputs) y efecto en el UI
        //Compra de peces
        if (datosjuego.GetEntradaCompraPecesNula()==true)
        {
            VerOpcionesCompraPeces(false);
        }
        else
        {
            VerOpcionesCompraPeces(true);
        }
        //Compra de alimento
        if (datosjuego.GetEntradaCompraAlimentoNula() == true)
        {
            VerOpcionesCompraAlimento(false);
        }
        else
        {
            VerOpcionesCompraAlimento(true);
        }
        //Suministro ración
        if (datosjuego.GetEntradaRacionNula() == true)
        {
            VerOpcionesSuministroRacion(false);
        }
        else
        {
            VerOpcionesSuministroRacion(true);
        }
        //Venta de peces
        if (datosjuego.GetEntradaVentaPecesNula()== true)
        {
            VerOpcionesVentaPeces(false);
        }
        else
        {
            VerOpcionesVentaPeces(true);
        }
        //Error dinero
        if (datosjuego.GetErrorDinero() == true)
        {
            imgErrorDineroInsuficiente.gameObject.SetActive(true);
        }
        //Error Lote
        if(datosjuego.GetErrorLote()==true)
        {
            imgErrorLimiteLote.gameObject.SetActive(true);
        }
        //Error Peces
        if(datosjuego.GetErrorPeces()==true)
        {
            imgErrorPecesInsuficientes.gameObject.SetActive(true);
        }
        //Error Alimento
        if (datosjuego.GetErrorAlimento() == true)
        {
            imgErrorAlimentoInsuficiente.gameObject.SetActive(true);
        }
        //Error Peso
        if (datosjuego.GetErrorPeso() == true)
        {
            imgErrorPesoMinimo.gameObject.SetActive(true);
        }
        //Error Conexión
        if (datosjuego.GetErrorConexion() == true)
        {
            imgErrorConexion.gameObject.SetActive(true);
        }
        //Resumen Producción
        //if (modelo.GetResumenProduccion()==true)
        //{
            ReproducirAnimacionResumenProduccion(modelo.GetResumenProduccion());
        //imgResumenProduccion.gameObject.SetActive(true);
        //}
        //Guardando partida
        if (datosjuego.GetGuardandoPartida())
        {
            imgGuardarEstado.gameObject.SetActive(true);
        }
        else
        {
            imgGuardarEstado.gameObject.SetActive(false);
        }

    }

    private void OnGUI()
    {
        nombreJugador.text = GestionBD.singleton.GetNombreJugador();
        nombreSesion.text = GestionBD.singleton.GetNombreSesion();
        HUDNivel.text = "NIVEL 4";
        HUDDinero.text = datosjuego.GetDineroDisponible().ToString();
        HUDAlimento.text = AjustarGramos(datosjuego.GetInventarioAlimento());
        PIPrecioPez.text = "$ " + datosjuego.GetValorUnitarioPez().ToString();
        PIPrecioAlimento.text = "$ "+datosjuego.GetValorUnitarioAlimento().ToString();
        PIInventarioAlimento.text = "Inventario Alimento: \n" + AjustarGramos(datosjuego.GetInventarioAlimento());
        PMPrecioKg.text = "$ " + datosjuego.GetPrecioVentaPez().ToString("N0");

        RPPecesVendidos.text = modelo.GetTotalPecesVendidos().ToString();
        RPVentas.text = "$ "+modelo.GetTotalVentas().ToString("N0");
        RPPecesIniciales.text = modelo.GetInventarioInicialPeces().ToString();
        RPCostoPeces.text = "$ " + modelo.GetCostoInicialPeces().ToString("N0");
        RPCostoMantenimiento.text = "$ " + modelo.GetCostoTotalMantenimientoDiario().ToString("N0");
        RPCostoAlimento.text = "$ " + modelo.GetCostoTotalAlimento().ToString("N0");
        RPTotalCostos.text = "$ " + modelo.GetCostosTotales().ToString("N0");
        RPGanancia.text = VerGanancia(RPGanancia,modelo.GetTotalVentas()-modelo.GetCostosTotales());
        //Actualización de textos e interacción en el juego
        if (modelo.GetProduccionActiva() == true)
        {
            //Actualización de datos del HUD
            HUDPeces.text = modelo.GetPecesEnLoteP().ToString();
            HUDDia.text = modelo.GetTiempoIteracion().ToString();
            //Actualización de datos de Paneles
            PIMensaje.text= "Tienes una producción\n activa.";
            PEPeso.text=AjustarGramos(modelo.GetPesoPromedio());
            PEConsumoActual.text = AjustarGramos(modelo.GetRacionAlimenticiaReal());
            PEFactorConversion.text = Math.Round(modelo.GetFactorConversion(),3).ToString();
            PEBiomasa.text = AjustarGramos(modelo.GetPesoPromedio()*modelo.GetPecesEnLoteP());
            PECostoKg.text = "$ " + Math.Round(((modelo.GetCostosTotales()) / ((modelo.GetPesoPromedio()/1000f) * modelo.GetPecesEnLoteP())),2).ToString("N0");

            PERacionRequerida.text = AjustarGramos(modelo.GetRacionRequerida());
            PERacionASuministrar.text = AjustarGramos(modelo.GetRacionAlimenticia());
            PERacionSuministrada.text = AjustarGramos(modelo.GetRacionSuministrada());
            PERacionLote.text = AjustarGramos(modelo.GetRacionSuministrada() * modelo.GetPecesEnLoteP());

            PEAreaEstanque.text= Math.Round(modelo.GetAreaEstanque(),2).ToString() + " m\xB2";
            PENivelOxigeno.text = Math.Round(modelo.GetNivelOxigeno(), 2).ToString() + " ppm";
            PEDensidadGramosPeces.text = Math.Round(modelo.GetDensidadGramosPeces(),3).ToString() + " g/m\xB2";
            PEPecesMuertos.text = modelo.GetPecesMuertos().ToString();

            PECostoPeces.text = "$ " + modelo.GetCostoInicialPeces().ToString("N0");
            PECostoMantenimiento.text = "$ " + modelo.GetCostoTotalMantenimientoDiario().ToString("N0");
            PECostoAlimentacion.text = "$ " + modelo.GetCostoTotalAlimento().ToString("N0");
            PETotalCostos.text = "$ " + modelo.GetCostosTotales().ToString("N0");

            PMCostosTotales.text = "$ " + modelo.GetCostosTotales().ToString("N0");
            PMBiomasa.text =  AjustarGramos(modelo.GetPesoPromedio()*modelo.GetPecesEnLoteP());
            PMCostoKg.text = "$ " + Math.Round(((modelo.GetCostosTotales()) / ((modelo.GetPesoPromedio()/1000f)*modelo.GetPecesEnLoteP())),2).ToString("N0");
            
            SDDValorDia.text = modelo.timer.GetDuracionDia().ToString() + " seg";
        }
        else
        {
            //Actualización de datos del HUD
            HUDPeces.text = "-";
            HUDDia.text = "-";
            HUDDia.gameObject.SetActive(false);
            HUDLogoPesco.gameObject.SetActive(true);
            
            //Actualización de datos de Paneles
            PEPeso.text = vacio;
            PEFactorConversion.text = vacio;
            PEConsumoActual.text = vacio;
            PETotalCostos.text = vacio;
            PECostoKg.text = vacio;
            PMCostosTotales.text = vacio;
            PMBiomasa.text = vacio;
            PMMensaje.text = "No tienes peces \n en tu producción.";
            PMCostoKg.text = vacio;
            PERacionRequerida.text = vacio;
            PERacionSuministrada.text = vacio;
            PERacionLote.text = vacio;
            //Actualización de botones
            if (modelo.GetResumenProduccion() == true)
            {
                btnUIComprarPeces.interactable = false;
            }
        }
    }

    private void IniciarAlerta(bool estado)
    {
        if (estado)
        {
            if (control)
            {
                StartCoroutine(Parpadeo());
                control = false;
            }
        }
        else
        {
            control = true;
        }
    }

    private void ReproducirAnimacionInicioProduccion(bool estado)
    {
        if (estado)
        {
            if (controlAnimacionProduccion)
            {
                StartCoroutine(IniciarAnimacionProduccion());
                controlAnimacionProduccion = false;
            }
        }
        else
        {
            controlAnimacionProduccion = true;
        }
    }

    private void ReproducirAnimacionResumenProduccion(bool estado)
    {
        if (estado)
        {
            if (controlAnimacionVenta)
            {
                StartCoroutine(IniciarAnimacionVenta());
                controlAnimacionVenta = false;
            }
        }
        else
        {
            controlAnimacionVenta = true;
        }
    }

    private void VerOpcionesCompraPeces(bool activo)
    {
        btnUIComprarPeces.gameObject.SetActive(activo);
    }

    private void VerOpcionesCompraAlimento(bool activo)
    {
        btnUIComprarAlimento.gameObject.SetActive(activo);
    }

    private void VerOpcionesSuministroRacion(bool activo)
    {
        btnCambiarRacion.gameObject.SetActive(activo);
    }

    private void VerOpcionesVentaPeces(bool activo)
    {
        btnUIVenderPeces.gameObject.SetActive(activo);       
    }

    //Función de parpadeo del texto de inventario;
    private IEnumerator Parpadeo()
    {
        //print("Corriendo rutina" + Time.time);
        while (true)
        {
            HUDAlimento.color = Color.red;
            yield return new WaitForSeconds(.5f);
            HUDAlimento.color = Color.black;
            yield return new WaitForSeconds(.5f);
            if (!alertaAlimentoActivada)break;
        }
        //print("Acabó la rutina" + Time.time);
    }

    private IEnumerator CambioColor(Text texto, bool caso)
    {
        //Bool del valor: 1-->Aumento, 0-->Disminución
        if (caso)
        {
            texto.color = Color.green;
            yield return new WaitForSeconds(8);
            texto.color = Color.black;
        }
        else
        {
            texto.color = Color.yellow;
            yield return new WaitForSeconds(.5f);
            texto.color = Color.black;
        }
        //yield return null;
    }

    IEnumerator IniciarAnimacionProduccion()
    {
        bool camaraActiva = false;
        if (canvasInsumos.gameObject.activeInHierarchy)
        {
            camaraActiva = true;
        }
        canvasInsumos.gameObject.SetActive(false);
        HUDDia.gameObject.SetActive(false);
        camaraEstanque.gameObject.SetActive(true);
        HUDLogoPesco.gameObject.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(3f);
        controlAnimacionProduccion = false;
        HUDLogoPesco.gameObject.SetActive(false);
        camaraEstanque.gameObject.SetActive(false);
        if (camaraActiva)
        {
            canvasInsumos.gameObject.SetActive(true);
        }
        HUDDia.gameObject.SetActive(true);
    }

    IEnumerator IniciarAnimacionVenta()
    {
        yield return new WaitForSeconds(4f);
        canvasMercado.gameObject.SetActive(false);
        camaraMercado.gameObject.SetActive(false);
        canvasResumen.gameObject.SetActive(true);
        camaraResumen.gameObject.SetActive(true);
    }

    public void CerrarVentanaErrorDineroInsuficiente()
    {
        imgErrorDineroInsuficiente.gameObject.SetActive(false);
        datosjuego.SetErrorDinero(false);
    }

    public void CerrarVentanaErrorLimiteLote()
    {
        imgErrorLimiteLote.gameObject.SetActive(false);
        datosjuego.SetErrorLote(false);
    }

    public void CerrarVentanaErrorPecesInsuficientes()
    {
        imgErrorPecesInsuficientes.gameObject.SetActive(false);
        datosjuego.SetErrorPeces(false);
    }

    public void CerrarVentanaErrorAlimentoInsuficiente()
    {
        imgErrorAlimentoInsuficiente.gameObject.SetActive(false);
        datosjuego.SetErrorAlimento(false);
    }

    public void CerrarVentanaErrorPesoMinimo()
    {
        imgErrorPesoMinimo.gameObject.SetActive(false);
        datosjuego.SetErrorPeso(false);
    }

    public void CerrarVentanaErrorConexion()
    {
        imgErrorConexion.gameObject.SetActive(false);
        datosjuego.SetErrorConexion(false);
    }

    public void CerrarVentanaResumenProduccion()
    {
        canvasResumen.gameObject.SetActive(false);
        camaraResumen.gameObject.SetActive(false);
        //imgResumenProduccion.gameObject.SetActive(false);
    }

    //Método para ajustar la escala de gramos
    private string AjustarGramos(double valor)
    {
        string mensaje="";
        if (valor < 1000)
        {
            mensaje = Math.Round(valor, 2).ToString() + " g";
        }
        else if(valor>=1000 && valor<1000000)
        {
            mensaje = Math.Round(valor / 1000f, 3).ToString() + " kg";
        }
        else if (valor>=1000000)
        {
            mensaje = Math.Round(valor / 1000000f, 3).ToString() + " t";
        }
        return mensaje;
    }

    //Método de redondeo de valores numéricos a sus enteros
    private string Redondeo(double valor)
    {
        string valorMostrado = "";
        valorMostrado=Math.Round(valor).ToString();
        return valorMostrado;
    }

    //Método para visualización de ganancia
    private string VerGanancia(Text texto, double valor)
    {
        string ganancia = "";
        if (valor > 0)
        {
            texto.color = Color.black;
            ganancia = "$ " + (modelo.GetTotalVentas() - modelo.GetCostosTotales()).ToString("N0");
        }
        else
        {
            texto.color = Color.red;
            ganancia = "- $ " + Math.Abs(modelo.GetTotalVentas() - modelo.GetCostosTotales()).ToString("N0");
        }
        return ganancia;
    }
}
