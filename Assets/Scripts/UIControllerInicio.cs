﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControllerInicio : MonoBehaviour {
    public InputField username;
    public InputField pass;
    public Image errorConexion;

    public void Conectar()
    {
        string us = username.text;
        string pw = pass.text;
        GestionBD.singleton.IniciarSesion(us, pw);
    }

    private void Update()
    {
        if (GestionBD.singleton.GetEstadoError())
        {
            errorConexion.gameObject.SetActive(true);
            GestionBD.singleton.SetEstadoError(false);
        }
    }
}
