﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DatosVenta{

    public string nomUsuario;
    public string nomSesion;
    public double pesoPez;
    public int numPeces;
    public double valorVenta;
    public string modVenta;
    public double costo;


    public void AsignarValores(string[] datos)
    {
        nomUsuario = datos[0];
        nomSesion = datos[1];
        pesoPez = double.Parse(datos[2]);
        numPeces = int.Parse(datos[3]);
        valorVenta = double.Parse(datos[4]);
        modVenta = datos[5];
        costo = double.Parse(datos[6]);
    }

    /*
    public void CargarDatos(string[] datos)
    {
        datos[0] = nombreJugador;
        datos[1] = sesionJugador;
        datos[2] = dineroDisponible.ToString();
        datos[3] = inventarioPeces.ToString();
        datos[4] = inventarioAlimento.ToString();
        datos[5] = tiempoIteracion.ToString();
        datos[6] = estadoProduccion.ToString();
        datos[7] = modalidadVenta;
        datos[8] = turnoVenta.ToString();
        datos[9] = unidoGremio.ToString();

        datos[10] = pesoPez.ToString();
        datos[11] = totalCostos.ToString();
        datos[12] = consumoReal.ToString();
        datos[13] = coberturaPesoMaximo.ToString();
        datos[14] = factorConversion.ToString();
        datos[15] = incrementoPesoPez.ToString();
        datos[16] = totalAlimentoSuministrado.ToString();
        datos[17] = incrementoGastos.ToString();
        datos[18] = temperatura.ToString();
        datos[19] = racionAlimenticiaRequerida.ToString();
        datos[20] = racionAlimenticiaASuministrar.ToString();
        datos[21] = racionAlimenticiaSuministrada.ToString();
        datos[22] = oxigenoDisuelto.ToString();
        datos[23] = pecesMuertos.ToString();
        datos[24] = costoInicialPeces.ToString();
        datos[25] = inventarioInicialPeces.ToString();
        datos[26] = costoMantenimientoDiario.ToString();
        datos[27] = costoTotalAlimento.ToString();
        datos[28] = totalVentas.ToString();
        datos[29] = totalPecesVendidos.ToString();
    }*/
}
