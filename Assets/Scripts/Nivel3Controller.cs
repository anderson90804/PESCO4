﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Nivel3Controller : MonoBehaviour {

    public bool juegoGanado = false;
    public GameLogicN3 datosjuego;
    public ModeloN3 modelo;
    //Declaración de variables de control para conocer si ha ganado el juego
    public double peso;
    public double inventarioPeces;
    public double inventarioAlimento;
    public double racionAlimenticia;
    public double dinero;
    //Control de misiones
    public bool mision1completa;
    public bool mision2completa;
    public bool mision3completa;
    //Avisos de juego ganado o perdido
    public GameObject levelWin;
    public GameObject levelOver;
    public GameObject vendedor;

    void Start()
    {
        mision1completa = false;
        mision2completa = false;
        mision3completa = false;
        datosjuego.GetDineroDisponible();
        modelo.GetModeloActual();
        levelWin.gameObject.SetActive(false);
        levelOver.gameObject.SetActive(false);
    }

    private void Update()
    {
        this.dinero = datosjuego.GetDineroDisponible();
        this.inventarioAlimento = datosjuego.GetInventarioAlimento();

        Debug.Log(datosjuego.GetInventarioAlimento(), this);
        Debug.Log("Produccion Activa: "+ modelo.GetProduccionActiva(), this);

        if (modelo.GetProduccionActiva())
        {
            this.peso = (modelo.GetPesoPromedio());
            this.inventarioPeces = (datosjuego.GetInventarioPeces());
            this.racionAlimenticia = (modelo.GetRacionAlimenticia());
            this.inventarioAlimento = (datosjuego.GetInventarioAlimento());

            Debug.Log(datosjuego.GetInventarioAlimento(), this);
            Debug.Log(this.inventarioAlimento, this);

        }

        if ((int)this.dinero < 1)
        {
            Debug.Log("Sin dinero", this);
            GameOver();
        }
        if ((int)this.inventarioAlimento < 1)
        {
            Debug.Log("Sin comida", this);
            GameOver();
        }

        //Validación misión 1
        if (((float)inventarioAlimento) > 0.01f)
        {
            mision1completa = true; 
        }
        //Validación de misión 2
        if (inventarioPeces >= 10)
        {
            mision2completa = true;
        }
        //Validación misión 3
        if (((float)racionAlimenticia) > 0.01f)
        {
            mision3completa = true;
        }
    }

    public void ValidarJuego()
    {
        if (mision1completa && mision2completa && mision3completa)
        {
            StartCoroutine(GameWin());
            GestionBD.singleton.RegistrarFinalizacionNivel();
        }
        else
        {
            GameOver();
        }
    }

    IEnumerator GameWin()
    {
        Debug.Log("GANASTE----------------->");
        vendedor.GetComponent<Animator>().SetTrigger("venta");
        yield return new WaitForSeconds(3f);
        GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>().enabled = false;
        levelWin.SetActive(true);
        juegoGanado = true;
    }

    private void GameOver()
    {
        if (GameObject.FindGameObjectWithTag("Player") != null) { 
            GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>().enabled = false;
        }
        levelOver.SetActive(true);
    }

}
