﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameLogicN7 : MonoBehaviour {

    /*En este sccript se colocan todas las acciones lógicas que producen un cambio en el juego en el nivel 6
    (Efectos de pulsar los botones)*/

    //Variables de control para los estados del juego
    public bool isPaused = false;
    public bool estadoProduccion = false;
    //Variables de control de errores
    private bool errorDinero = false;
    private bool errorPeces = false;
    private bool errorAlimento = false;
    private bool errorPeso = false;
    private bool errorLote = false;
    private bool errorPesoPez = false;
    private bool errorConexion = false;
    private bool errorTurno = false;
    private bool errorGremioPeso = false;
    private bool errorGremioConexion = false;
    private bool errorGremioPeces = false;
    private bool errorGremioCupo = false;
    private bool errorLimiteGremio = false;
    private int LIMITEPECES;
    private bool alertaAlimento = false;
    private bool variacionDinero = false;
    private int casoDinero;
    [SerializeField]
    private bool guardandoPartida = false;
    [SerializeField]
    [Tooltip("Control para saber si hay datos registrados en la BD")]
    public string[] registroPartida;
    public string[] datosPartida;
    public string[] datosVenta;
    public string[] datosOferta;

    public DatosJuego gameData= new DatosJuego();
    public DatosVenta ventaData = new DatosVenta();

    //Variables de operación para el juego
    [SerializeField]
    private int pecesAComprar;
    private double gramosAlimentoAComprar;
    [SerializeField]
    private float gramosAlimentoASuministrar;
    private int pecesAVender;
    [SerializeField]
    private double totalCompraPeces;
    private double totalCompraAlimento;
    private double totalVentaPeces;
    public GameObject produccion;
    public GameObject vendedorMercado;
    public GameObject vendedorGremio;

    //Datos del jugador
    [SerializeField]
    private string nombreJugador;
    private string sesionJugador;
    private double dineroDisponible;
    private int inventarioPeces;
    private double inventarioAlimento;
    private string modalidadVenta;
    private int turnoVentaAsignado;
    private bool unidoGremio;

    //Precios del nivel
    private int VALORUNITARIOPEZ;
    private int VALORUNITARIOALIMENTO;
    [SerializeField]
    private double precioVentaKgPez;
    private float PESOMINIMO;
    private double precioVentaKgGremio;
    private double multaGremio;

    //Datos del servidor
    private int tiempoMercado;
    private int turnoServidor;
    private int minimoOferentes;
    private int limiteOferentes;
    private double cantidadRequeridaGremio;
    private double coberturaOfertaGremio;
    private int cantidadActualOferentes;

    //Elementos UI necesarios para capturar los valores de operación y calcular totales
    //Compra de peces
    public InputField entradaCompraPeces;
    public Text PIValorTotalPeces;
    //Compra de alimento
    public InputField entradaCompraAlimento;
    public Text PIValorTotalAlimento;
    //Suministro de ración
    public InputField entradaRacion;
    //Venta de peces 
    //Venta libre
    public InputField entradaVLVentaPeces;
    public Text PMVLValorTotalVenta;
    //Venta por turnos
    public Dropdown listadoSeleccionTurno;
    public InputField entradaVTVentaPeces;
    public Text PMVTValorTotalVenta;
    //Venta por gremio
    public InputField entradaVGVentaPeces;
    public Text PMVGValorTotalVenta;

    //Variables de control sobre entradas nulas o no válidas
    private bool entradaCompraPecesNula = true;
    private bool entradaCompraAlimentoNula = true;
    private bool entradaRacionNula = true;
    private bool entradaVLVentaPecesNula = true;
    private bool entradaVTVentaPecesNula = true;
    private bool entradaVGVentaPecesNula = true;

    private void Awake()
    {
        Time.timeScale = 1;
        VALORUNITARIOPEZ = 150;
        VALORUNITARIOALIMENTO = 2;
        LIMITEPECES = 10000;
        PESOMINIMO = 0.125f;
        precioVentaKgPez = 8800;
        dineroDisponible = 100000;
        turnoVentaAsignado = 0;
        turnoServidor = 1;
        unidoGremio = false;
        precioVentaKgGremio = 10000;
        multaGremio = 1500;
        //oferta del gremio
        //minimoOferentes = 3;
        //limiteOferentes = 5;
        //cantidadRequeridaGremio = 5000;
        //coberturaOfertaGremio = 0;
        //cantidadActualOferentes = 5;

    }

    // Use this for initialization
    void Start()
    {
        isPaused = false;
        CalcularValorCompraPeces();
        CalcularValorCompraAlimento();
        CalcularValorSuministroRacion();
        CalcularValorVentaPeces();
        CalcularValorVentaPecesPorTurno();
        CalcularValorVentaPecesPorGremio();

        modalidadVenta = "indefinida";
        nombreJugador = GestionBD.singleton.GetNombreJugador();
        sesionJugador = GestionBD.singleton.GetNombreSesion();
        tiempoMercado = GestionBD.singleton.GetTiempoMercado();

        datosPartida = new string[30];
        registroPartida = new string[30];
        datosVenta = new string[7];
        datosOferta = new string[7];
        //produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
        produccion.SetActive(false);
        //Traer datos de la BD de datos en caso de que existan
        CargarPartida();

        StartCoroutine(DatosOferta());
        ConsultarPrecio();
    }

    // Update is called once per frame
    void Update()
    {
        //La lógica del juego debe estar mirando si hay una producción activa.
        estadoProduccion = produccion.GetComponent<ModeloN7>().GetProduccionActiva();
        ControlAlimento();
        if (Input.GetKeyDown("p"))
        {
            Pause();
        }
    }

    //Función para Pausar
    public void Pause()
    {
        if (isPaused == true)//¿El juego está pausado?
        {//Hagalo correr
            Time.timeScale = 1;
            isPaused = false;
        }
        else if (isPaused == false)
        {//Hagalo pausar
            Time.timeScale = 0;
            isPaused = true;
        }
    }

    //Método para efectuar compra de peces, calcula si hay dinero para la transacción y toma el número de peces a comprar para realizar la compra
    public void ComprarPeces()
    //Si hay produccion activa no puedo comprar;
    {
        bool excesoPeces = (pecesAComprar > LIMITEPECES) ? true : false;//Limite de peces que caben en el estanque
        if (!excesoPeces)
        {
            double subtotal = dineroDisponible - totalCompraPeces;
            if (subtotal >= 0)
            {
                //Modificación de los inventarios
                SetDineroDisponible(subtotal);
                SetInventarioPeces(pecesAComprar);
                CambioDinero(0);
                Debug.Log("Compra de peces efectuada a los " + Time.time + ".");
                IniciarProduccion(pecesAComprar, totalCompraPeces);
                SalvarEstado();
            }
            else
            {
                //Ventana no alcanza el dinero
                errorDinero = true;
                //print("No le alcanza el dinero para comprar los peces");
            }
        }
        else
        {
            //Ventana No puede comprar más de esos peces
            errorLote = true;
        }
        entradaCompraPeces.text = null;
    }

    //Método para efectuar compra de alimento, calcula si hay dinero para la transacción y toma el número de gramos a comprar para realizar la compra
    public void ComprarAlimento()
    {
        double nuevoInventarioAlimento = inventarioAlimento + gramosAlimentoAComprar;
        double subtotal = dineroDisponible - totalCompraAlimento;
        if ((subtotal) > 0)
        {
            //Modificación de los inventarios
            SetDineroDisponible(subtotal);
            SetInventarioAlimento(nuevoInventarioAlimento);
            CambioDinero(0);
            Debug.Log("Compra de alimento efectuada a los " + Time.time + ".");
            SalvarEstado();
        }
        else
        {
            //Ventana no alcanza el dinero
            errorDinero = true;
            //print("No le alcanza el dinero para comprar alimento");
        }
        entradaCompraAlimento.text = null;
    }

    //Método que calcula el valor a pagar por un determinado número de peces, toma el contenido del input y multiplica por el precio del pez
    public void CalcularValorCompraPeces()
    {
        if (entradaCompraPeces.text != "" && !(entradaCompraPeces.text.Contains("-")))
        {
            int.TryParse(entradaCompraPeces.text, out pecesAComprar);
            if (pecesAComprar > 0)
            {
                totalCompraPeces = VALORUNITARIOPEZ * pecesAComprar;
                PIValorTotalPeces.text = "$ " + totalCompraPeces.ToString("N0");
                entradaCompraPecesNula = false;
            }
        }
        else
        {
            entradaCompraPecesNula = true;
            PIValorTotalPeces.text = "Valor a pagar";
        }
    }

    //Método que calcula el valor a pagar por el alimento de  los peces, toma el contenido del input y multiplica por el precio del gramo de alimento
    public void CalcularValorCompraAlimento()
    {
        if (entradaCompraAlimento.text != "" && !(entradaCompraAlimento.text.Contains("-")))
        {
            double.TryParse(entradaCompraAlimento.text, out gramosAlimentoAComprar);
            if (gramosAlimentoAComprar > 0)
            {
                totalCompraAlimento = VALORUNITARIOALIMENTO * gramosAlimentoAComprar;
                PIValorTotalAlimento.text = AjustarGramos(gramosAlimentoAComprar).ToString() + "\n" + "$ " + totalCompraAlimento.ToString("N0");
                entradaCompraAlimentoNula = false;
            }
        }
        else
        {
            entradaCompraAlimentoNula = true;
            PIValorTotalAlimento.text = "Valor a pagar";
        }
    }

    //Método para cambiar la ración de alimento que se suministra al estanque
    public void CambiarRacion()
    {
        //double subtotal = dineroDisponible - totalCompraAlimento;
        double racionLote = gramosAlimentoASuministrar * inventarioPeces;
        double nuevoInventarioAlimento = inventarioAlimento - racionLote;
        if ((nuevoInventarioAlimento) >= 0)
        {
            //Modificación de los inventarios
            //SetDineroDisponible(subtotal);
            //SetInventarioAlimento(nuevoInventarioAlimento);
            CambiarRacionAlimenticia(gramosAlimentoASuministrar);
            Debug.Log("Modificación de la ración de alimento efectuada a los " + Time.time + ".");
            SalvarEstado();
        }
        else
        {
            //Ventana no alcanza el alimento
            errorAlimento = true;
            //print("No le alcanza el inventario de alimento para suministrar la ración.");
        }
        entradaRacion.text = null;
    }

    public void CalcularValorSuministroRacion()
    {
        if (entradaRacion.text != "" && !(entradaRacion.text.Contains("-")))
        {
            float.TryParse(entradaRacion.text, out gramosAlimentoASuministrar);
            entradaRacionNula = false;
        }
        else
        {
            entradaRacionNula = true;
        }
    }

    public void UnirLibre()
    {
        this.modalidadVenta = "libre";
        SalvarEstado();
    }

    //Método para efectuar venta de peces, valida el inventario de peces y realiza operaciones dependiendo si es una venta parcial (una parte del lote) o total del lote.
    public void VenderPeces()
    {
        //Guardo los datos de venta y luego acabo el modelo y los peces en el estanque
        print("Comparación-->"+((float)PesoPez() - PESOMINIMO));
        //Reinicio el tiempo
        if ((float)PesoPez() - PESOMINIMO > 0.0001f)
        {
            if (inventarioPeces >= pecesAVender)
            {
                double subtotal = dineroDisponible + totalVentaPeces;
                int nuevoInventarioPeces = inventarioPeces - pecesAVender;
                //¿Es una venta parcial?
                if (pecesAVender != inventarioPeces)
                {
                    SalvarVenta();
                    //Modificación de inventarios
                    SetInventarioPeces(nuevoInventarioPeces);
                    SetDineroDisponible(subtotal);
                    CambioDinero(1);
                    //Actualización de inventario de peces en el modelo
                    ModificarProduccion(inventarioPeces);
                    Debug.Log("Venta parcial realizada a los " + Time.time + ".");
                    vendedorMercado.GetComponent<Animator>().SetTrigger("venta");
                    SalvarEstado();
                }
                else if (pecesAVender == inventarioPeces)//¿Es una venta total?
                {
                    SalvarVenta();
                    if (GestionBD.singleton.GetEstadoVenta())
                    {
                        //Modificación de inventarios
                        SetInventarioPeces(nuevoInventarioPeces);
                        SetDineroDisponible(subtotal);
                        CambioDinero(1);
                        //Actualización de datos en el modelo
                        FinalizarProduccion(nuevoInventarioPeces);
                        estadoProduccion = false;
                        Debug.Log("Venta total realizada a los " + Time.time + ".");
                        vendedorMercado.GetComponent<Animator>().SetTrigger("venta");
                        SalvarEstado();
                    }
                    else
                    {
                        errorConexion = true;
                    }

                }
            }
            else
            {
                //Ventana no alcanza los peces del inventario
                errorPeces = true;
                print("No le alcanzan los peces");
            }
        }
        else
        {
            errorPeso = true;
            print("Los peces no tienen el peso mínimo");
        }
        entradaVLVentaPeces.text = null;
    }

    //Función que calcula el valor a pagar por un determinado número de peces, toma el contenido del input y multiplica por el precio del pez
    public void CalcularValorVentaPeces()
    {
        if (entradaVLVentaPeces.text != "")
        {
            //peces = Int32.Parse(entradaCompraPeces.text);
            int.TryParse(entradaVLVentaPeces.text, out pecesAVender);
            if (pecesAVender > 0)
            {
                double biomasaVenta = pecesAVender * PesoPez();
                totalVentaPeces = precioVentaKgPez * biomasaVenta;
                PMVLValorTotalVenta.text = biomasaVenta.ToString() + " kg \n" + "$ " + totalVentaPeces.ToString("N0");
                entradaVLVentaPecesNula = false;
            }
        }
        else
        {
            entradaVLVentaPecesNula = true;
            PMVLValorTotalVenta.text = "Total de la venta";
        }
    }

    public void AsignarTurnoVenta()
    {
        this.turnoVentaAsignado = listadoSeleccionTurno.value + 1;
        modalidadVenta = "turno";
        SalvarEstado();
    }

    public void VenderPecesPorTurno()
    {
        //Guardo los datos de venta y luego acabo el modelo y los peces en el estanque
        //Reinicio el tiempo
        if (turnoVentaAsignado == turnoServidor)
        {
            if ((float)PesoPez() - PESOMINIMO > 0.01f)
            {
                if (inventarioPeces >= pecesAVender)
                {
                    double subtotal = dineroDisponible + totalVentaPeces;
                    int nuevoInventarioPeces = inventarioPeces - pecesAVender;
                    //¿Es una venta parcial?
                    if (pecesAVender != inventarioPeces)
                    {
                        SalvarVenta();
                        if (GestionBD.singleton.GetEstadoVenta())
                        {
                            //Modificación de inventarios
                            SetInventarioPeces(nuevoInventarioPeces);
                            SetDineroDisponible(subtotal);
                            CambioDinero(1);
                            //Actualización de inventario de peces en el modelo
                            ModificarProduccion(inventarioPeces);
                            Debug.Log("Venta parcial realizada a los " + Time.time + ".");
                            vendedorMercado.GetComponent<Animator>().SetTrigger("venta");
                            SalvarEstado();
                        }
                        else
                        {
                            errorConexion = true;
                        }
                    }
                    else if (pecesAVender == inventarioPeces)//¿Es una venta total?
                    {
                        SalvarVenta();
                        if (GestionBD.singleton.GetEstadoVenta())
                        {
                            //Modificación de inventarios
                            SetInventarioPeces(nuevoInventarioPeces);
                            SetDineroDisponible(subtotal);
                            CambioDinero(1);
                            //Actualización de datos en el modelo
                            FinalizarProduccion(nuevoInventarioPeces);
                            estadoProduccion = false;
                            Debug.Log("Venta total realizada a los " + Time.time + ".");
                            vendedorMercado.GetComponent<Animator>().SetTrigger("venta");
                            SalvarEstado();
                        }
                        else
                        {
                            errorConexion = true;
                        }
                            
                    }
                }
                else
                {
                    //Ventana no alcanza los peces del inventario
                    errorPeces = true;
                    print("No le alcanzan los peces");
                }
            }
            else
            {
                errorPeso = true;
                print("Los peces no tienen el peso mínimo");
            }
        }
        else
        {
            errorTurno = true;
            print("No se encuentra en su turno para vender");
        }
        entradaVTVentaPeces.text = null;
    }

    public void CalcularValorVentaPecesPorTurno()
    {
        if (entradaVTVentaPeces.text != "")
        {
            //peces = Int32.Parse(entradaCompraPeces.text);
            int.TryParse(entradaVTVentaPeces.text, out pecesAVender);
            if (pecesAVender > 0)
            {
                double biomasaVenta = pecesAVender * PesoPez();
                totalVentaPeces = precioVentaKgPez * biomasaVenta;
                PMVTValorTotalVenta.text = biomasaVenta.ToString() + " kg \n" + "$ " + totalVentaPeces.ToString("N0");
                entradaVTVentaPecesNula = false;
            }
        }
        else
        {
            entradaVTVentaPecesNula = true;
            PMVTValorTotalVenta.text = "Total de la venta";
        }
    }


    public void UnirGremio()
    {
        //Consultar cantidad actual de oferentes
        if (cantidadActualOferentes < limiteOferentes)
        {
            unidoGremio = true;
            modalidadVenta = "gremio";
            //Registrar ingreso al gremio
            GestionBD.singleton.VincularGremio();
            SalvarEstado();

        }
        else
        {
            errorGremioCupo = true;
            print("No hay cupo en el gremio");
        }
    }

    public void VenderPecesPorGremio()
    {
        //Guardo los datos de venta y luego acabo el modelo y los peces en el estanque
        //Reinicio el tiempo
        if (unidoGremio)
        {
            if ((float)PesoPez() - PESOMINIMO > 0.01f)
            {
                if (inventarioPeces >= pecesAVender)
                {
                    double subtotal = dineroDisponible + totalVentaPeces;
                    int nuevoInventarioPeces = inventarioPeces - pecesAVender;
                    //¿Es una venta parcial?
                    if (pecesAVender != inventarioPeces)
                    {
                        SalvarVenta();
                        if (GestionBD.singleton.GetEstadoVenta())
                        {
                            //Modificación de inventarios
                            SetInventarioPeces(nuevoInventarioPeces);
                            SetDineroDisponible(subtotal);
                            CambioDinero(1);
                            //Actualización de inventario de peces en el modelo
                            ModificarProduccion(inventarioPeces);
                            Debug.Log("Venta parcial realizada a los " + Time.time + ".");
                            vendedorGremio.GetComponent<Animator>().SetTrigger("venta");
                            SalvarEstado();
                        }
                        else
                        {
                            errorConexion = true;
                        }  
                    }
                    else if (pecesAVender == inventarioPeces)//¿Es una venta total?
                    {
                        SalvarVenta();
                        if (GestionBD.singleton.GetEstadoVenta())
                        {
                            //Modificación de inventarios
                            SetInventarioPeces(nuevoInventarioPeces);
                            SetDineroDisponible(subtotal);
                            CambioDinero(1);
                            //Actualización de datos en el modelo
                            FinalizarProduccion(nuevoInventarioPeces);
                            estadoProduccion = false;
                            Debug.Log("Venta total realizada a los " + Time.time + ".");
                            vendedorGremio.GetComponent<Animator>().SetTrigger("venta");
                            SalvarEstado();
                        }
                        else
                        {
                            errorConexion = true;
                        }
                        
                    }
                }
                else
                {
                    //Ventana no alcanza los peces del inventario
                    errorGremioPeces = true;
                    print("No le alcanzan los peces");
                }
            }
            else
            {
                errorGremioPeso = true;
                print("Los peces no tienen el peso mínimo");
            }
        }
        else
        {
            print("No se encuentra vinculado al gremio");
        }
        entradaVGVentaPeces.text = null;
    }

    public void CalcularValorVentaPecesPorGremio()
    {
        if (entradaVGVentaPeces.text != "")
        {
            //peces = Int32.Parse(entradaCompraPeces.text);
            int.TryParse(entradaVGVentaPeces.text, out pecesAVender);
            if (pecesAVender > 0)
            {
                double biomasaVenta = pecesAVender * PesoPez();
                totalVentaPeces = precioVentaKgGremio * biomasaVenta;
                PMVGValorTotalVenta.text = biomasaVenta.ToString() + " kg \n" + "$ " + totalVentaPeces.ToString("N0");
                entradaVGVentaPecesNula = false;
            }
        }
        else
        {
            entradaVGVentaPecesNula = true;
            PMVGValorTotalVenta.text = "Total de la venta";
        }
    }

    public void AbandonarGremio()
    {
        double nuevoSaldo = dineroDisponible - multaGremio;
        SetDineroDisponible(nuevoSaldo);
        this.unidoGremio = false;
        this.modalidadVenta = "indefinida";
        //Registrar salida del gremio
        GestionBD.singleton.DesvincularGremio();
        SalvarEstado();
    }

    // ---MÉTODOS DE MODIFICACIÓN DEL MODELO DE PRODUCCIÓN---
    private void IniciarProduccion(int peces, double costoInicial)
    {
        //Activación de una nueva producción
        produccion.SetActive(true);
        produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
        produccion.GetComponent<ModeloN7>().timer.CorrerTiempo();
        produccion.GetComponent<ModeloN7>().SetCostoInicialPeces(costoInicial);
        produccion.GetComponent<ModeloN7>().SetCostosTotales(costoInicial);
        produccion.GetComponent<ModeloN7>().SetProduccionActiva(true);
        produccion.GetComponent<ModeloN7>().SetInventarioInicialPeces(peces);
        produccion.GetComponent<ModeloN7>().SetPecesenLoteP(peces);
        //produccion.GetComponent<LotePecesController>().SetPezCount(peces);    
    }

    private void ModificarProduccion(int peces)
    {
        //Modificación de la producción activa
        produccion.GetComponent<ModeloN7>().SetPecesenLoteP(peces);
        //produccion.GetComponent<LotePecesController>().SetPezCount(peces);
        produccion.GetComponent<ModeloN7>().SumarVenta(totalVentaPeces, pecesAVender);
    }

    private void FinalizarProduccion(int peces)
    {
        //Finalización de la producción activa
        produccion.GetComponent<ModeloN7>().SetProduccionActiva(false);
        produccion.GetComponent<ModeloN7>().SetPecesenLoteP(peces);
        //produccion.GetComponent<LotePecesController>().SetPezCount(peces);
        produccion.GetComponent<ModeloN7>().SumarVenta(totalVentaPeces, pecesAVender);
        //produccion.SetActive(false);
        produccion.GetComponent<ModeloN7>().SetResumenProduccion(true);
    }

    private void CambiarRacionAlimenticia(double nuevaracion)
    {
        produccion.GetComponent<ModeloN7>().SetRacionAlimenticiaP(nuevaracion);
        CalcularEscalaAlimento(nuevaracion);
    }

    //Función para ajustar visualmente el tamaño de la ración por medio de la escala del GameObject
    private void CalcularEscalaAlimento(double racionActual)
    {
        if (racionActual == 0)
        {
            produccion.GetComponent<LotePecesController>().goal.GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            produccion.GetComponent<LotePecesController>().goal.GetComponent<MeshRenderer>().enabled = true;
            if (racionActual <= 1)
            {
                produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            }
            else
            {
                if (racionActual <= 2)
                {
                    produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                }
                else
                {
                    if (racionActual <= 3)
                    {
                        produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                    }
                    else
                    {
                        if (racionActual <= 4)
                        {
                            produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                        }
                        else
                        {
                            produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        }
                    }
                }
            }
        }
    }

    //Método para convertir el peso promedio del pez en kg
    private double PesoPez()
    {
        double peso = produccion.GetComponent<ModeloN7>().GetPesoPromedio();
        return Math.Round((peso / 1000), 3);
    }

    private void ControlAlimento()
    {
        if (inventarioAlimento < 100)
        {
            alertaAlimento = true;
        }
        else
        {
            alertaAlimento = false;
        }
        //Visualización del alimento dentro del estanque
        if (estadoProduccion == true)
        {
            if (inventarioAlimento == 0)
            {
                produccion.GetComponent<LotePecesController>().goal.GetComponent<MeshRenderer>().enabled = false;
                CambiarRacionAlimenticia(0);
            }
        }
    }

    private void CambioDinero(int tipo)
    {
        variacionDinero = true;
        casoDinero = tipo;
    }

    //Método para ajustar la escala de gramos
    private string AjustarGramos(double valor)
    {
        string mensaje = "";
        if (valor < 1000)
        {
            mensaje = Math.Round(valor, 2).ToString() + " g";
        }
        else if (valor >= 1000 && valor < 1000000)
        {
            mensaje = Math.Round(valor / 1000f, 3).ToString() + " kg";
        }
        else if (valor >= 1000000)
        {
            mensaje = Math.Round(valor / 1000000f, 3).ToString() + " t";
        }
        return mensaje;
    }

    // --- METODOS GETER AND SETER ----

    // --- Metodos Getter ---

    public string GetNombreJugador()
    {
        return this.nombreJugador;
    }


    public string GetNombreSesion()
    {
        return this.sesionJugador;
    }

    public int GetInventarioPeces()
    {
        return this.inventarioPeces;
    }

    public double GetInventarioAlimento()
    {
        return this.inventarioAlimento;
    }

    public double GetDineroDisponible()
    {
        return this.dineroDisponible;
    }

    public int GetValorUnitarioPez()
    {
        return this.VALORUNITARIOPEZ;
    }

    public int GetValorUnitarioAlimento()
    {
        return this.VALORUNITARIOALIMENTO;
    }

    public double GetPrecioVentaPez()
    {
        return this.precioVentaKgPez;
    }

    public int GetTurnoVentaServidor()
    {
        return this.turnoServidor;
    }

    public int GetTurnoVentaAsignado()
    {
        return this.turnoVentaAsignado;
    }

    public double GetPrecioVentaGremio()
    {
        return this.precioVentaKgGremio;
    }

    public double GetMultaGremio()
    {
        return this.multaGremio;
    }

    public string GetModalidadVenta()
    {
        return this.modalidadVenta;
    }

    public bool GetErrorDinero()
    {
        return this.errorDinero;
    }

    public bool GetErrorLote()
    {
        return this.errorLote;
    }

    public bool GetErrorAlimento()
    {
        return this.errorAlimento;
    }

    public bool GetErrorPeces()
    {
        return this.errorPeces;
    }

    public bool GetErrorPeso()
    {
        return this.errorPeso;
    }

    public bool GetErrorTurno()
    {
        return this.errorTurno;
    }

    public bool GetErrorConexion()
    {
        return this.errorConexion;
    }

    public bool GetErrorGremioPeces()
    {
        return this.errorGremioPeces;
    }

    public bool GetErrorGremioPeso()
    {
        return this.errorGremioPeso;
    }

    public bool GetErrorGremioConexion()
    {
        return this.errorGremioConexion;
    }

    public bool GetErrorGremioCupo()
    {
        return this.errorGremioCupo;
    }

    public bool GetAlertaAlimento()
    {
        return this.alertaAlimento;
    }

    public bool GetVariacionDinero()
    {
        return this.variacionDinero;
    }

    public bool GetGuardandoPartida()
    {
        return this.guardandoPartida;
    }

    public int GetCasoDinero()
    {
        return this.casoDinero;
    }

    public bool GetEntradaCompraPecesNula()
    {
        return this.entradaCompraPecesNula;
    }

    public bool GetEntradaCompraAlimentoNula()
    {
        return this.entradaCompraAlimentoNula;
    }

    public bool GetEntradaRacionNula()
    {
        return this.entradaRacionNula;
    }

    public bool GetEntradaVLVentaPecesNula()
    {
        return this.entradaVLVentaPecesNula;
    }

    public bool GetEntradaVTVentaPecesNula()
    {
        return this.entradaVTVentaPecesNula;
    }

    public bool GetEstadoProduccion()
    {
        return this.estadoProduccion;
    }

    public bool GetEstadoGremio()
    {
        return this.unidoGremio;
    }

    public int GetMinimoParticipantesGremio()
    {
        return this.minimoOferentes;
    }

    public int GetLimiteParticipantesGremio()
    {
        return this.limiteOferentes;
    }

    public double GetCantidadRequeridaGremio()
    {
        return this.cantidadRequeridaGremio;
    }

    public int GetCantidadActualOferentes()
    {
        return this.cantidadActualOferentes;
    }

    // --- Metodos Setter ---
    public void SetInventarioPeces(int peces)
    {
        this.inventarioPeces = peces;
        produccion.GetComponent<LotePecesController>().SetPezCount(peces);
    }

    public void SetInventarioAlimento(double gramos)
    {
        this.inventarioAlimento = gramos;
    }

    public void SetDineroDisponible(double saldo)
    {
        this.dineroDisponible = saldo;
    }

    public void SetErrorDinero(bool estado)
    {
        this.errorDinero = estado;
    }

    public void SetErrorLote(bool estado)
    {
        this.errorLote = estado;
    }

    public void SetErrorPeces(bool estado)
    {
        this.errorPeces = estado;
    }

    public void SetErrorAlimento(bool estado)
    {
        this.errorAlimento = estado;
    }

    public void SetErrorTurno(bool estado)
    {
        this.errorTurno = estado;
    }

    public void SetErrorPeso(bool estado)
    {
        this.errorPeso = estado;
    }

    public void SetErrorConexion(bool estado)
    {
        this.errorConexion = estado;
    }

    public void SetErrorGremioPeces(bool estado)
    {
        this.errorGremioPeces = estado;
    }

    public void SetErrorGremioPeso(bool estado)
    {
        this.errorGremioPeso = estado;
    }

    public void SetErrorGremioConexion(bool estado)
    {
        this.errorGremioConexion = estado;
    }

    public void SetErrorGremioCupo(bool estado)
    {
        this.errorGremioCupo = estado;
    }

    public void SetVariacionDinero(bool estado)
    {
        this.variacionDinero=estado;
    }

    // --- METODOS PARA MANEJO DE ESCENAS ----

    public void ReiniciarNivel()
    {
        //Eliminar datos
        GestionBD.singleton.EliminarEstadoJuego();
        //Recargue la escena
        Scene nivelActual = SceneManager.GetActiveScene();
        Debug.Log("Reiniciando el " + nivelActual.name);
        Time.timeScale = 1;
        SceneManager.LoadScene(nivelActual.name, LoadSceneMode.Single);
    }

    public void VolverHome()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuPrincipal");
    }

    // --- METODOS PARA MANEJO DE DATOS ----

    //Función para cargar los datos del juego para continuar jugando
    private void CargarPartida()
    {
        StartCoroutine(CargarEstadoPartida());
    }

    private IEnumerator CargarEstadoPartida()
    {
        yield return new WaitForSeconds(1);
        //Aquí debe actualizar los datos del jugador, modelo y peces en pantalla junto a su tamaño de acuerdo al peso
        if (GestionBD.singleton.GetRegistroData())
        {
            GestionBD.singleton.registroGameData.CargarDatos(registroPartida);
            //Consultar de BD el último registro
            produccion.SetActive(true);
            yield return new WaitForSeconds(1);
            this.nombreJugador = registroPartida[0];
            this.sesionJugador = registroPartida[1];
            SetDineroDisponible(double.Parse(registroPartida[2]));
            SetInventarioPeces(int.Parse(registroPartida[3]));
            produccion.GetComponent<ModeloN7>().SetPecesenLoteP(int.Parse(registroPartida[3]));
            //produccion.GetComponent<LotePecesController>().SetPezCount(int.Parse(registro[3]));
            SetInventarioAlimento(double.Parse(registroPartida[4]));
            produccion.GetComponent<ModeloN7>().SetTiempoIteracion(int.Parse(registroPartida[5]));
            produccion.GetComponent<PlayTimer>().SetTotalDias(int.Parse(registroPartida[5]));
            this.estadoProduccion = bool.Parse(registroPartida[6]);
            this.modalidadVenta = registroPartida[7];
            this.turnoVentaAsignado = int.Parse(registroPartida[8]);
            this.unidoGremio = bool.Parse(registroPartida[9]);

            string[] datosParaModelo = new string[produccion.GetComponent<ModeloN7>().datosModelo.Length];
            Array.Copy(registroPartida, 10, datosParaModelo, 0, datosParaModelo.Length);
            produccion.GetComponent<ModeloN7>().SetDatosModelo(datosParaModelo);

            produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
            produccion.GetComponent<ModeloN7>().timer.CorrerTiempo();

            print("Actualizados los datos.--------------");
            GestionBD.singleton.SetRegistroData(false);
        }
    }

    //Función para guardar el estado del juego
    public void SalvarEstado()
    {
        StartCoroutine(GuardarPartida());
    }

    private IEnumerator GuardarPartida()
    {
        guardandoPartida = true;
        GenerarDatosPartida();
        gameData.AsignarValores(this.datosPartida);
        string json = JsonUtility.ToJson(gameData);
        print(json);
        GestionBD.singleton.GuardarEstadoJuego(json);
        print("Guardado de datos realizado con éxito" + Time.time);
        yield return new WaitForSeconds(1);
        guardandoPartida = false;
    }

    //Función para guardar los datos de la partida
    private void GenerarDatosPartida()
    {
        /*Estructura de partida
         * [0] User
         * [1] Sesion
         * [2] Dinero
         * [3] Inventario peces
         * [4] Inventario alimento
         * [5] Dias transcurridos
         * [6] Estado produccion
         * [7] Tiempo iteracion
         * [8] Turno de venta
         * [9] Unión a gremio
         * [10-29] Datos del modelo
         */
        datosPartida[0] = nombreJugador;
        datosPartida[1] = sesionJugador;
        datosPartida[2] = dineroDisponible.ToString();
        datosPartida[3] = inventarioPeces.ToString();
        datosPartida[4] = inventarioAlimento.ToString();
        datosPartida[5] = (produccion.GetComponent<ModeloN7>().GetTiempoIteracion()).ToString();
        datosPartida[6] = estadoProduccion.ToString();
        datosPartida[7] = modalidadVenta;
        datosPartida[8] = turnoVentaAsignado.ToString();
        datosPartida[9] = unidoGremio.ToString();
        if (this.estadoProduccion)
        {
            for (int i = 0; i < produccion.GetComponent<ModeloN7>().datosModelo.Length; i++)
            {
                datosPartida[10 + i] = produccion.GetComponent<ModeloN7>().datosModelo[i];
            }
        }
        else
        {
            for (int j = 10; j < 30; j++)
            {
                datosPartida[j] = 0.ToString();
            }
        }      
    }

    public void SalvarVenta()
    {
        StartCoroutine(GuardarVenta());
    }

    private IEnumerator GuardarVenta()
    {
        guardandoPartida = true;
        GenerarDatosVenta();
        ventaData.AsignarValores(this.datosVenta);
        string jsonventa = JsonUtility.ToJson(ventaData);
        print(jsonventa);
        GestionBD.singleton.GuardarVenta(jsonventa);
        if (GestionBD.singleton.GetEstadoVenta())
        {
            print("Guardado de venta realizado con éxito" + Time.time);
        }
        yield return new WaitForSeconds(1);
        guardandoPartida = false;
    }

    //Función para guardar los datos de venta
    private void GenerarDatosVenta()
    {
        /*Estructura de venta
         * [0] User
         * [1] Sesion
         * [2] Peso
         * [3] Número de peces 
         * [4] Valor de la venta
         * [5] Modalidad de venta
         * [6] Costo acumulado
         */
        datosVenta[0] = nombreJugador;
        datosVenta[1] = sesionJugador;
        datosVenta[2] = produccion.GetComponent<ModeloN7>().GetPesoPromedio().ToString();
        datosVenta[3] = pecesAVender.ToString();
        datosVenta[4] = totalVentaPeces.ToString();
        datosVenta[5] = modalidadVenta;
        datosVenta[6] = produccion.GetComponent<ModeloN7>().GetCostosTotales().ToString();
    }

    //Función para consultar el precio del mercado
    public void ConsultarPrecio()
    {
        StartCoroutine(PrecioMercado());
        print("Consultando precio del mercado...");
    }

    private IEnumerator PrecioMercado()
    {
        while (true)
        {
            GestionBD.singleton.ConsultarDatosMercado();
            yield return new WaitForSeconds(1f);
            this.precioVentaKgPez = GestionBD.singleton.GetPrecioMercado();
            this.turnoServidor = GestionBD.singleton.GetTurnoMercado();
            yield return new WaitForSeconds(tiempoMercado);
        }
    }

    //Función para consulta de datos de oferta
    private IEnumerator DatosOferta()
    {
        while (true)
        {
            GestionBD.singleton.ConsultarOfertaGremio();
            if (GestionBD.singleton.GetRegistroOferta())
            {
                datosOferta = GestionBD.singleton.GetDatosOferta();
                cantidadRequeridaGremio = double.Parse(datosOferta[0]);
                precioVentaKgGremio = double.Parse(datosOferta[1]);
                minimoOferentes = int.Parse(datosOferta[2]);
                multaGremio = double.Parse(datosOferta[3]);
                limiteOferentes = int.Parse(datosOferta[4]);
                cantidadActualOferentes = int.Parse(datosOferta[5]);
                coberturaOfertaGremio = double.Parse(datosOferta[6]);
            }
            yield return new WaitForSeconds(10f);
        }
    }

    public void RegistrarIntegranteGremio()
    {
        GestionBD.singleton.VincularGremio();
    }
}
