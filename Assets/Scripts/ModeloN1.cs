﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ModeloN1 : MonoBehaviour
{
    //-->Variables de gestión del juego con Unity
    public GameLogicN1 datosjuego;
    private bool produccionActiva = false;
    //Ayudas visuales
    public Image PesoLoadingBar;
    private float pesoControl = 500f;
    private bool controlBarra;
    public Image barraPeso;
    public Sprite barraPrimerRango;
    public Sprite barraSegundoRango;
    //Campos generales
    public int dato;
    public PlayTimer timer;
    private int tiempoIteracion = 0;
    public int actSize = 1;
    public string[] datosModelo = new string[20];

    //Declaración parámetros (Propios de cada nivel)
    private double factorMaximoConversionP = 0.92;
    private double factorMaximoProduccionP = 0.7;
    private double pesoMaximoP = 1200;
    private double gastoFijoDiario = 0.5;
    private double costoInicialPez=100;
    private int pecesEnLoteP=1;

    //-->Variables del modelo Pesco Pez
    private string sp = " ";
    //Variables
    [SerializeField]
    private double racionAlimenticiaRequeridaP;
    private double racionAlimenticiaSuministradaP;
    [SerializeField]
    private double factorConversionP;
    private double fraccionMantenimiento;
    [SerializeField]
    private double consumoRealP;
    [SerializeField]
    private double consumoMantenimiento;
    [SerializeField]
    private double consumoProduccion;
    [SerializeField]
    private double coberturaPesoMaximoP;
    //  Flujos
    private double incrementoPesoPezP;
    // Niveles
    [SerializeField]
    private double pesoPezP;

    //-->Sector de alimentación
    //Variables
    private double racionAlimenticiaASuministrarP;
    //Niveles
    private double totalAlimentoSuministrado;

    //-->Sector de costos del pez
    //Flujos
    private double incrementoGastosP;
    //Niveles
    private double totalCostosP;

    //-->Sector de ventas y utilidades
    //Variable
    private double utilidad;
    //Niveles
    private double totalVenta;

    //Llamar información necesaria de otros scripts
    private void Awake()
    {
        timer = GetComponent<PlayTimer>();
    }

    //Método que se ejecuta cuando es llamado por primera vez el script, también cuando se ha puesto Enable el objeto.
    private void Start()
    {
        produccionActiva = true;
        StartCoroutine(ValoresPorDefecto());
        StartCoroutine(GetDatosModelo());
        produccionActiva = true;
        controlBarra = true;
    }

    /*Aquí se colocan todas las funciones que requieran de actualizar en referencia 
    a eventos que han sucedido, en qué momento se hace cambios de simulación.*/
    void Update()
    {
        if (produccionActiva == true)
        {
            dato = timer.GetTotalDias();
            if (tiempoIteracion != dato)
            {
                //Debug.Log("Cambió el día");
                StartCoroutine(ActualizarSimulacion(dato - tiempoIteracion));
                StartCoroutine(GetDatosModelo());
                tiempoIteracion = dato;
                datosjuego.SalvarEstado();
                //Crecimiento de peces
                CrecerPeces();
                if (((float)pesoPezP - pesoControl) < 0.01f)
                {
                    PesoLoadingBar.fillAmount = (float)(pesoPezP / pesoControl);
                }
                else
                {
                    CambiarRangoBarra();
                    PesoLoadingBar.fillAmount = (((float)pesoPezP - pesoControl) / pesoControl);
                }
            }
        }
        else if (produccionActiva == false)
        {
            tiempoIteracion = 0;
            StartCoroutine(ReiniciarSimulacion());
            timer.ReiniciarTiempo();
        }

    }

    private void CrecerPeces()
    {
        gameObject.GetComponent<LotePecesController>().SetPezPeso(this.pesoPezP);
    }

    //Función para cambiar fondo de barra del peso
    private void CambiarRangoBarra()
    {
        if (controlBarra)
        {
            if (((float)pesoPezP - pesoControl) < 0.01f)
            {
                barraPeso.GetComponent<Image>().sprite = barraPrimerRango;
            }
            else
            {
                barraPeso.GetComponent<Image>().sprite = barraSegundoRango;
            }
            controlBarra = false;
        }
    }

    IEnumerator ReiniciarSimulacion()
    {

        StartCoroutine(ValoresPorDefecto());
        StartCoroutine(GetDatosModelo()); ;
        controlBarra = true;
        CambiarRangoBarra();
        //Debug.Log("Empezamos de cero...");
        yield return null;
    }

    IEnumerator ActualizarSimulacion(int iteraciones)
    {
        for (int i = 1; i <= iteraciones; i++)
        {
            StartCoroutine(IteracionModelo());
            tiempoIteracion = tiempoIteracion + iteraciones;
        }
        yield return null;
    }

    /**
     * Reestablece los valores por defecto de las variables implicadas en la simulación
     */
    IEnumerator ValoresPorDefecto()
    {
        pesoPezP = 1;
        coberturaPesoMaximoP = pesoPezP/pesoMaximoP;
        racionAlimenticiaRequeridaP = pesoPezP * (PescoMultiplicadorPez.CalcTasaAlimentacion(pesoPezP)/100);
        racionAlimenticiaASuministrarP = racionAlimenticiaRequeridaP;
        racionAlimenticiaSuministradaP = 0.0;
        factorConversionP = 0.0;
        fraccionMantenimiento = 0.0;
        consumoRealP = 0.0;
        consumoMantenimiento = 0.0;
        consumoProduccion = 0.0;
        totalAlimentoSuministrado = 0.0;
        incrementoPesoPezP = 0.0;
        totalCostosP = costoInicialPez;
        incrementoGastosP = 0.0;
        yield return null;
    }

    /**
     * Este método ejecuta las ecuaciones del modelo, representa una iteración, al
     * ser ejecutado varias veces representa las iteraciones del modelo
     */
    IEnumerator IteracionModelo()
    {
        
        factorConversionP= factorMaximoConversionP * PescoMultiplicadorPez.MultiplicadorFactorConversion(coberturaPesoMaximoP);
        fraccionMantenimiento = 1 - (factorMaximoProduccionP * PescoMultiplicadorPez.MultiplicadorFactorConversion(coberturaPesoMaximoP));
        consumoMantenimiento = racionAlimenticiaRequeridaP * fraccionMantenimiento;
        racionAlimenticiaSuministradaP = racionAlimenticiaASuministrarP;
        totalAlimentoSuministrado = totalAlimentoSuministrado + racionAlimenticiaSuministradaP;
        consumoRealP = racionAlimenticiaRequeridaP;
        consumoProduccion = (consumoRealP - consumoMantenimiento < 0) ? 0.0 : consumoRealP - consumoMantenimiento;
        incrementoPesoPezP = consumoProduccion * factorConversionP;
        pesoPezP = pesoPezP + incrementoPesoPezP;
        incrementoGastosP = gastoFijoDiario;
        totalCostosP = totalCostosP + incrementoGastosP;
        datosjuego.SetDineroDisponible(datosjuego.GetDineroDisponible() - incrementoGastosP);
        coberturaPesoMaximoP = pesoPezP / pesoMaximoP;
        racionAlimenticiaRequeridaP = pesoPezP * (PescoMultiplicadorPez.CalcTasaAlimentacion(pesoPezP) / 100);
        racionAlimenticiaASuministrarP = racionAlimenticiaRequeridaP;
        //Debug.Log(racionAlimenticia + sp + coverturaPesoMaximo + sp + factorConversion + sp + incrementoPesoPez + sp + pesoPez + sp + gastoComida + sp + incrementoGastos + sp + costos);
        yield return null;
    }

    public void SumarVenta(double valorVenta)
    {
        totalVenta = totalVenta + valorVenta;
    }

    // --- METODOS GETER AND SETER ----

    // --- Metodos Getter ---

    //Método que permite actualizar los datos del modelo después de efectuada una iteración
    IEnumerator GetDatosModelo()
    {
        this.datosModelo[0] = pesoPezP.ToString();
        this.datosModelo[1] = totalCostosP.ToString();
        this.datosModelo[2] = consumoRealP.ToString();
        this.datosModelo[3] = coberturaPesoMaximoP.ToString();
        this.datosModelo[4] = factorConversionP.ToString();
        this.datosModelo[5] = incrementoPesoPezP.ToString();
        this.datosModelo[6] = totalAlimentoSuministrado.ToString();
        this.datosModelo[7] = incrementoGastosP.ToString();
        this.datosModelo[8] = 0.ToString();
        this.datosModelo[9] = racionAlimenticiaRequeridaP.ToString();
        this.datosModelo[10] = racionAlimenticiaASuministrarP.ToString();
        this.datosModelo[11] = racionAlimenticiaSuministradaP.ToString();
        this.datosModelo[12] = 0.ToString();
        this.datosModelo[13] = 0.ToString();
        this.datosModelo[14] = costoInicialPez.ToString();
        this.datosModelo[15] = 0.ToString();
        this.datosModelo[16] = 0.ToString();
        this.datosModelo[17] = 0.ToString();
        this.datosModelo[18] = 0.ToString();
        this.datosModelo[19] = 0.ToString();
        yield return datosModelo;
    }

    public double GetPesoPromedio()
    {
        return this.pesoPezP;
    }

    public double GetFactorConversion()
    {
        return this.factorConversionP;
    }

    public double GetCostoProduccion()
    {
        return this.totalCostosP;
    }

    public double GetRacionAlimenticia()
    {
        return this.racionAlimenticiaASuministrarP;
    }

    public double GetRacionRequerida()
    {
        return this.racionAlimenticiaRequeridaP;
    }

    public double GetRacionSuministrada()
    {
        return this.racionAlimenticiaSuministradaP;
    }

    public double GetConsumoActual()
    {
        return this.consumoRealP;
    }

    public double GetTotalAlimentoSuministrado()
    {
        return this.totalAlimentoSuministrado;
    }

    public int GetPecesEnLoteP()
    {
        return this.pecesEnLoteP;
    }

    public int GetTiempoIteracion()
    {
        return this.tiempoIteracion;
    }

    public bool GetProduccionActiva()
    {
        return this.produccionActiva;
    }

    public ModeloN1 GetModeloActual()
    {
        return this;
    }


    // --- Metodos Setter ---
    //Método que sirve para cargar datos (si existen) antes de iniciar la partida
    public void SetDatosModelo(string[] datosModelo)
    {
        pesoPezP = double.Parse(datosModelo[0]);
        totalCostosP = double.Parse(datosModelo[1]);
        consumoRealP = double.Parse(datosModelo[2]);
        coberturaPesoMaximoP = double.Parse(datosModelo[3]);
        factorConversionP = double.Parse(datosModelo[4]);
        incrementoPesoPezP = double.Parse(datosModelo[5]);
        totalAlimentoSuministrado = double.Parse(datosModelo[6]);
        incrementoGastosP = double.Parse(datosModelo[7]);

        //temperatura = double.Parse(datosModelo[8]);
        racionAlimenticiaRequeridaP = double.Parse(datosModelo[9]);
        racionAlimenticiaASuministrarP = double.Parse(datosModelo[10]);
        racionAlimenticiaSuministradaP = double.Parse(datosModelo[11]);

        //oxigenoDisuelto = double.Parse(datosModelo[12]);
        //pecesMuertos = int.Parse(datosModelo[13]);

        costoInicialPez = double.Parse(datosModelo[14]);
        //inventarioInicialPeces = int.Parse(datosModelo[15]);
        //costoMantenimientoDiario = double.Parse(datosModelo[16]);
        //costoTotalAlimento = double.Parse(datosModelo[17]);
        //totalVentas = double.Parse(datosModelo[18]);
        //totalPecesVendidos = int.Parse(datosModelo[19]);
    }

    public void SetPecesenLoteP(int peces)
    {
        this.pecesEnLoteP = peces;
    }

    public void SetProduccionActiva(bool estado)
    {
        produccionActiva = estado;
    }

    public void SetTiempoIteracion(int tiempo)
    {
        this.tiempoIteracion = tiempo;
    }
}