﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraCrecimiento : MonoBehaviour {

    public GameObject produccion;
    public Image LoadingBar;
    public Transform txtTiempoIteracion;
    [SerializeField]
    private float valorReferencia;
    [SerializeField]
    private float valorActual;

    // Use this for initialization
    void Start () {
        LoadingBar = this.GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
            LoadingBar.fillAmount = 1 - (valorActual / valorReferencia);
    }

}

