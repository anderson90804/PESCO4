﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PescoMultiplicadorEstanque : MonoBehaviour {

    private static double factorConversion;
    private static double efectoDensidad;
    private static double Ci;
    private static double Bi;
    private static double Ai;
    private static double yi;
    private static double ti;
    private static double x;
    private static double tasaMortalidadEfectoOxigeno;
    private static double efectoTemperatura;
    private static double tasaDiariaAlimentacion;

    /**Devuelve valor multiplicador del factor de conversión,
     * Uso de interpolación lineal con extremos
     * @param coverturaPesoMaximo
     * @return factorConversión
     */

    public static double MultiplicadorFactorConversion(double coberturaPesoMaximo)
    {
        if (coberturaPesoMaximo >= 0 && coberturaPesoMaximo < 0.1)
        {
            factorConversion = 1;
        }
        if (coberturaPesoMaximo >= 0.1 && coberturaPesoMaximo < 0.2)
        {
            factorConversion = ((coberturaPesoMaximo - 0.1) / 0.1) * (0.9707317 - 1) + 1;
        }
        if (coberturaPesoMaximo >= 0.2 && coberturaPesoMaximo < 0.3)
        {
            factorConversion = ((coberturaPesoMaximo - 0.2) / 0.1) * (0.9219512 - 0.9707317) + 0.9707317;
        }
        if (coberturaPesoMaximo >= 0.3 && coberturaPesoMaximo < 0.4)
        {
            factorConversion = ((coberturaPesoMaximo - 0.3) / 0.1) * (0.7853659 - 0.9219512) + 0.9219512;
        }
        if (coberturaPesoMaximo >= 0.4 && coberturaPesoMaximo < 0.5)
        {
            factorConversion = ((coberturaPesoMaximo - 0.4) / 0.1) * (0.595122 - 0.7853659) + 0.7853659;
        }
        if (coberturaPesoMaximo >= 0.5 && coberturaPesoMaximo < 0.6)
        {
            factorConversion = ((coberturaPesoMaximo - 0.5) / 0.1) * (0.297561 - 0.595122) + 0.595122;
        }
        if (coberturaPesoMaximo >= 0.6 && coberturaPesoMaximo < 0.7)
        {
            factorConversion = ((coberturaPesoMaximo - 0.6) / 0.1) * (0.1463415 - 0.297561) + 0.297561;
        }
        if (coberturaPesoMaximo >= 0.7 && coberturaPesoMaximo < 0.8)
        {
            factorConversion = ((coberturaPesoMaximo - 0.7) / 0.1) * (0.06341463 - 0.1463415) + 0.1463415;
        }
        if (coberturaPesoMaximo >= 0.8 && coberturaPesoMaximo < 0.9)
        {
            factorConversion = ((coberturaPesoMaximo - 0.8) / 0.1) * (0.02926829 - 0.06341463) + 0.06341463;
        }
        if (coberturaPesoMaximo >= 0.9 && coberturaPesoMaximo < 1)
        {
            factorConversion = ((coberturaPesoMaximo - 0.9) / 0.1) * (0 - 0.02926829) + 0.02926829;
        }
        if (coberturaPesoMaximo >= 1)
        {
            factorConversion = 0;
        }

        return factorConversion;
    }

    /**
     * Devuelve valor MultiplicadorModeloPeces EfectoDensidad,
     * Uso de interpolación mediante Splines Cúbicos,
     * valores Calculados previamente y de acuerdo al
     * modelo para ahorrar cálculos en tiempo de ejecución.
     * @param PesoPez
     * @return EfectoPeso
     */

    public static double CalcEfectoDensidad(double DensidadDeGPeces)
    {

        x = DensidadDeGPeces;

        if (x >= 0 && x < 100)
        {
            Ci = 0.00077000;
            Bi = 0;
            Ai = -0.00000001;
            yi = 0;
            ti = 0;
        }
        if (x >= 100 && x < 200)
        {
            Ci = 0.00061121;
            Bi = -0.00000159;
            Ai = 0;
            yi = 0.07170732;
            ti = 100;
        }
        if (x >= 200 && x < 300)
        {
            Ci = 0.00016903;
            Bi = -0.00000283;
            Ai = 0.00000001;
            yi = 0.11279640;
            ti = 200;
        }
        if (x >= 300 && x < 400)
        {
            Ci = 0.00003902;
            Bi = 0.00000153;
            Ai = 0;
            yi = 0.1159198;
            ti = 300;
        }
        if (x >= 400 && x < 500)
        {
            Ci = 0.00023708;
            Bi = 0.00000045;
            Ai = -0.00000001;
            yi = 0.1315369;
            ti = 400;
        }
        if (x >= 500 && x < 600)
        {
            Ci = 0.00013708;
            Bi = -0.00000145;
            Ai = 0.00000001;
            yi = 0.15340090;
            ti = 500;
        }
        if (x >= 600 && x < 700)
        {
            Ci = 0.00005792;
            Bi = 0.00000066;
            Ai = -0.00000001;
            yi = 0.15964770;
            ti = 600;
        }
        if (x >= 700 && x < 800)
        {
            Ci = -0.00008766;
            Bi = -0.00000211;
            Ai = 0.00000001;
            yi = 0.16277120;
            ti = 700;
        }
        if (x >= 800 && x < 900)
        {
            Ci = -0.00009183;
            Bi = 0.00000207;
            Ai = -0.00000001;
            yi = 0.14682930;
            ti = 800;
        }
        if (x >= 900 && x < 1000)
        {
            Ci = 0.00007918;
            Bi = -0.00000036;
            Ai = 0.00000001;
            yi = 0.15024390;
            ti = 900;
        }
        if (x >= 1000 && x < 1100)
        {
            Ci = 0.00018487;
            Bi = 0.00000142;
            Ai = -0.00000001;
            yi = 0.16048780;
            ti = 1000;
        }
        if (x >= 1100 && x < 1200)
        {
            Ci = 0.00020573;
            Bi = -0.00000121;
            Ai = -0.00000001;
            yi = 0.18439020;
            ti = 1100;
        }
        if (x >= 1200 && x < 1300)
        {
            Ci = -0.00018827;
            Bi = -0.00000273;
            Ai = 0.00000005;
            yi = 0.18780490;
            ti = 1200;
        }
        if (x >= 1300 && x < 1400)
        {
            Ci = 0.00085468;
            Bi = 0.00001316;
            Ai = -0.00000006;
            yi = 0.19463410;
            ti = 1300;
        }
        if (x >= 1400 && x < 1500)
        {
            Ci = 0.00178907;
            Bi = -0.00000382;
            Ai = 0;
            yi = 0.35512200;
            ti = 1400;
        }
        if (x >= 1500 && x < 1600)
        {
            Ci = 0.00115004;
            Bi = -0.00000257;
            Ai = 0.00000001;
            yi = 0.50000000;
            ti = 1500;
        }
        if (x >= 1600 && x < 1700)
        {
            Ci = 0.00095713;
            Bi = 0.00000064;
            Ai = 0;
            yi = 0.60000000;
            ti = 1600;
        }
        efectoDensidad = yi + (x - ti) * (Ci + (x - ti) * (Bi + (x - ti) * Ai));
        return efectoDensidad;
    }
    
    /**
     * Devuelve valor multiplicador EfectoOxigeno,
     * Uso de Interpolación Lineal valores calculados previamente
     * de acuerdo a los valores del modelo.
     * @param OxigenoDisuelto
     * @return EfectoOxigeno
     */

    public static double CalcTasaMortalidadFaltaOxigeno(double OxigenoDisuelto)
    {
        if (OxigenoDisuelto >= 0 && OxigenoDisuelto < 0.5){
            tasaMortalidadEfectoOxigeno = ((OxigenoDisuelto - 0) / 0.5) * (0.15 - 0.2) + 0.2;
        }
        if (OxigenoDisuelto >= 0.5 && OxigenoDisuelto < 1){
            tasaMortalidadEfectoOxigeno = ((OxigenoDisuelto - 0.5) / 0.5) * (0.03121951 - 0.15) + 0.15;
        }
        if (OxigenoDisuelto >= 1 && OxigenoDisuelto < 1.5){
            tasaMortalidadEfectoOxigeno = ((OxigenoDisuelto - 1) / 0.5) * (0 - 0.03121951) + 0.03121951;
        }
        if (OxigenoDisuelto >= 1.5){
            tasaMortalidadEfectoOxigeno = 0;
        }

        return tasaMortalidadEfectoOxigeno;
    }
      
    /**
     * Devuelve valor MultiplicadorModeloPeces EfectoTemperatura,
     * Función de interpolación por pasos
     * @param Temperatura
     * @return EfectoTemperatura
     */
    public static double EfectTempAlim(double Temperatura)
    {
        if(Temperatura >= 16 && Temperatura < 17){
            efectoTemperatura = 0;
        }
        if (Temperatura >= 17 && Temperatura < 18)
        {
            efectoTemperatura = 0.06341463;
        }
        if (Temperatura >= 18 && Temperatura < 19)
        {
            efectoTemperatura = 0.1804878;
        }
        if (Temperatura >= 19 && Temperatura < 20)
        {
            efectoTemperatura = 0.395122;
        }
        if (Temperatura >= 20 && Temperatura < 21)
        {
            efectoTemperatura = 0.902439;
        }
        if (Temperatura >= 21 && Temperatura < 25)
        {
            efectoTemperatura = 1;
        }
        if (Temperatura >= 25 && Temperatura < 26)
        {
            efectoTemperatura = 0.8878049;
        }
        if (Temperatura >= 26 && Temperatura < 27)
        {
            efectoTemperatura = 0.6341463;
        }
        if (Temperatura >= 27 && Temperatura < 28)
        {
            efectoTemperatura = 0.4;
        }
        if (Temperatura >= 28 && Temperatura < 29)
        {
            efectoTemperatura = 0.2146341;
        }
        if (Temperatura >= 29 && Temperatura < 30)
        {
            efectoTemperatura = 0.126829268;
        }
        if (Temperatura >= 30 && Temperatura < 31)
        {
            efectoTemperatura = 0.06829268;
        }
        if (Temperatura >= 31 && Temperatura < 32)
        {
            efectoTemperatura = 0.01463415;
        }
        if (Temperatura >= 32)
        {
            factorConversion = 0;
        }
        return efectoTemperatura;
    }

    /**
     * Devuelve valor MultiplicadorModeloPeces TasaDiariaDeAlimentacion,
     * Uso de interpolación Lineal con extremos,
     * de acuerdo a los valores del modelo.
     * @param PesoPez
     * @@return TasaDiariaAlimentacion
     */

    public static double CalcTasaAlimentacion(double pesoPez)
    {
        //Peso entre 1 y 101 gramos
        if (pesoPez >= 1 && pesoPez < 6)
        {
            tasaDiariaAlimentacion = ((pesoPez - 1) / 5) * (10 - 15) + 15; ;
        }
        if (pesoPez >= 6 && pesoPez < 11)
        {
            tasaDiariaAlimentacion = ((pesoPez - 6) / 5) * (8 - 10) + 10;
        }
        if (pesoPez >= 11 && pesoPez < 16)
        {
            tasaDiariaAlimentacion = ((pesoPez - 11) / 5) * (7.849268 - 8) + 8;
        }
        if (pesoPez >= 16 && pesoPez < 21)
        {
            tasaDiariaAlimentacion = ((pesoPez - 16) / 5) * (7.849268 - 7.849268) + 7.849268;
        }
        if (pesoPez >= 21 && pesoPez < 26)
        {
            tasaDiariaAlimentacion = ((pesoPez - 21) / 5) * (7.448293 - 7.849268) + 7.849268;
        }
        if (pesoPez >= 26 && pesoPez < 31)
        {
            tasaDiariaAlimentacion = ((pesoPez - 26) / 5) * (7 - 7.448293) + 7.448293;
        }
        if (pesoPez >= 31 && pesoPez < 36)
        {
            tasaDiariaAlimentacion = ((pesoPez - 31) / 5) * (7 - 7) + 7;
        }
        if (pesoPez >= 36 && pesoPez < 41)
        {
            tasaDiariaAlimentacion = ((pesoPez - 36) / 5) * (6.713171 - 7) + 7;
        }
        if (pesoPez >= 41 && pesoPez < 46)
        {
            tasaDiariaAlimentacion = ((pesoPez - 41) / 5) * (6.245366 - 6.713171) + 6.713171;
        }
        if (pesoPez >= 46 && pesoPez < 51)
        {
            tasaDiariaAlimentacion = ((pesoPez - 46) / 5) * (5 - 6.245366) + 6.245366;
        }
        if (pesoPez >= 51 && pesoPez < 56)
        {
            tasaDiariaAlimentacion = ((pesoPez - 51) / 5) * (4.9 - 5) + 5;
        }
        if (pesoPez >= 56 && pesoPez < 61)
        {
            tasaDiariaAlimentacion = ((pesoPez - 56) / 5) * (4.507805 - 4.9) + 4.9;
        }
        if (pesoPez >= 61 && pesoPez < 66)
        {
            tasaDiariaAlimentacion = ((pesoPez - 61) / 5) * (4.1 - 4.507805) + 4.507805;
        }
        if (pesoPez >= 66 && pesoPez < 71)
        {
            tasaDiariaAlimentacion = ((pesoPez - 66) / 5) * (4 - 4.1) + 4.1;
        }
        if (pesoPez >= 71 && pesoPez < 76)
        {
            tasaDiariaAlimentacion = ((pesoPez - 71) / 5) * (3.705854 - 4) + 4;
        }
        if (pesoPez >= 76 && pesoPez < 81)
        {
            tasaDiariaAlimentacion = ((pesoPez - 76) / 5) * (3.6 - 3.705854) + 3.705854;
        }
        if (pesoPez >= 81 && pesoPez < 86)
        {
            tasaDiariaAlimentacion = ((pesoPez - 81) / 5) * (3.4 - 3.6) + 3.6;
        }
        if (pesoPez >= 86 && pesoPez < 91)
        {
            tasaDiariaAlimentacion = ((pesoPez - 86) / 5) * (3.3- 3.4) + 3.4;
        }
        if (pesoPez >= 91 && pesoPez < 96)
        {
            tasaDiariaAlimentacion = ((pesoPez - 91) / 5) * (3 - 3.3) + 3.3;
        }
        if (pesoPez >= 96 && pesoPez < 101)
        {
            tasaDiariaAlimentacion = ((pesoPez - 96) / 5) * (2.9 - 3) + 3;
        }

        //Peso entre 101 y 201 gramos
        if (pesoPez >= 101 && pesoPez < 106)
        {
            tasaDiariaAlimentacion = ((pesoPez - 101) / 5) * (2.9 - 2.9) + 2.9;
        }
        if (pesoPez >= 106 && pesoPez < 111)
        {
            tasaDiariaAlimentacion = ((pesoPez - 106) / 5) * (2.8 - 2.9) + 2.9;
        }
        if (pesoPez >= 111 && pesoPez < 116)
        {
            tasaDiariaAlimentacion = ((pesoPez - 111) / 5) * (2.8 - 2.8) + 2.8;
        }
        if (pesoPez >= 116 && pesoPez < 121)
        {
            tasaDiariaAlimentacion = ((pesoPez - 116) / 5) * (2.7 - 2.8) + 2.8;
        }
        if (pesoPez >= 121 && pesoPez < 126)
        {
            tasaDiariaAlimentacion = ((pesoPez - 121) / 5) * (2.7 - 2.7) + 2.7;
        }
        if (pesoPez >= 126 && pesoPez < 131)
        {
            tasaDiariaAlimentacion = ((pesoPez - 126) / 5) * (2.6 - 2.7) + 2.7;
        }
        if (pesoPez >= 131 && pesoPez < 136)
        {
            tasaDiariaAlimentacion = ((pesoPez - 131) / 5) * (2.6 - 2.6) + 2.6;
        }
        if (pesoPez >= 136 && pesoPez < 141)
        {
            tasaDiariaAlimentacion = ((pesoPez - 136) / 5) * (2.55 - 2.6) + 2.6;
        }
        if (pesoPez >= 141 && pesoPez < 146)
        {
            tasaDiariaAlimentacion = ((pesoPez - 141) / 5) * (2.53 - 2.55) + 2.55;
        }
        if (pesoPez >= 146 && pesoPez < 151)
        {
            tasaDiariaAlimentacion = ((pesoPez - 146) / 5) * (2.5 - 2.53) + 2.53;
        }
        if (pesoPez >= 151 && pesoPez < 156)
        {
            tasaDiariaAlimentacion = ((pesoPez - 151) / 5) * (2.4 - 2.5) + 2.5;
        }
        if (pesoPez >= 156 && pesoPez < 161)
        {
            tasaDiariaAlimentacion = ((pesoPez - 156) / 5) * (2.4 - 2.4) + 2.4;
        }
        if (pesoPez >= 161 && pesoPez < 165)
        {
            tasaDiariaAlimentacion = ((pesoPez - 161) / 5) * (2.35 - 2.4) + 2.4;
        }
        if (pesoPez >= 165 && pesoPez < 171)
        {
            tasaDiariaAlimentacion = ((pesoPez - 166) / 5) * (2.3 - 2.35) + 2.35;
        }
        if (pesoPez >= 171 && pesoPez < 176)
        {
            tasaDiariaAlimentacion = ((pesoPez - 171) / 5) * (2.29 - 2.3) + 2.3;
        }
        if (pesoPez >= 176 && pesoPez < 181)
        {
            tasaDiariaAlimentacion = ((pesoPez - 176) / 5) * (2.26 - 2.29) + 2.29;
        }
        if (pesoPez >= 181 && pesoPez < 186)
        {
            tasaDiariaAlimentacion = ((pesoPez - 181) / 5) * (2.23 - 2.26) + 2.26;
        }
        if (pesoPez >= 186 && pesoPez < 191)
        {
            tasaDiariaAlimentacion = ((pesoPez - 186) / 5) * (2.21 - 2.23) + 2.23;
        }
        if (pesoPez >= 191 && pesoPez < 196)
        {
            tasaDiariaAlimentacion = ((pesoPez - 191) / 5) * (2.21 - 2.21) + 2.21;
        }
        if (pesoPez >= 196 && pesoPez < 201)
        {
            tasaDiariaAlimentacion = ((pesoPez - 196) / 5) * (2.2 - 2.21) + 2.21;
        }

        //Peso entre 201 y 301 gramos
        if (pesoPez >= 201 && pesoPez < 206)
        {
            tasaDiariaAlimentacion = ((pesoPez - 201) / 5) * (2.15 - 2.2) + 2.2;
        }
        if (pesoPez >= 206 && pesoPez < 211)
        {
            tasaDiariaAlimentacion = ((pesoPez - 206) / 5) * (2.15 - 2.15) + 2.15;
        }
        if (pesoPez >= 211 && pesoPez < 216)
        {
            tasaDiariaAlimentacion = ((pesoPez - 211) / 5) * (2.14 - 2.15) + 2.15;
        }
        if (pesoPez >= 216 && pesoPez < 221)
        {
            tasaDiariaAlimentacion = ((pesoPez - 216) / 5) * (2.12 - 2.14) + 2.14;
        }
        if (pesoPez >= 221 && pesoPez < 226)
        {
            tasaDiariaAlimentacion = ((pesoPez - 221) / 5) * (2.1 - 2.12) + 2.12;
        }
        if (pesoPez >= 226 && pesoPez < 231)
        {
            tasaDiariaAlimentacion = ((pesoPez - 226) / 5) * (2 - 2.1) + 2.1;
        }
        if (pesoPez >= 231 && pesoPez < 236)
        {
            tasaDiariaAlimentacion = ((pesoPez - 231) / 5) * (2 - 2) + 2;
        }
        if (pesoPez >= 236 && pesoPez < 241)
        {
            tasaDiariaAlimentacion = ((pesoPez - 236) / 5) * (1.98 - 2) + 2;
        }
        if (pesoPez >= 241 && pesoPez < 246)
        {
            tasaDiariaAlimentacion = ((pesoPez - 241) / 5) * (1.97 - 1.98) + 1.98;
        }
        if (pesoPez >= 246 && pesoPez < 251)
        {
            tasaDiariaAlimentacion = ((pesoPez - 246) / 5) * (1.96 - 1.97) + 1.97;
        }
        if (pesoPez >= 251 && pesoPez < 256)
        {
            tasaDiariaAlimentacion = ((pesoPez - 251) / 5) * (1.95 - 1.96) + 1.96;
        }
        if (pesoPez >= 256 && pesoPez < 261)
        {
            tasaDiariaAlimentacion = ((pesoPez - 256) / 5) * (1.93 - 1.95) + 1.95;
        }
        if (pesoPez >= 261 && pesoPez < 266)
        {
            tasaDiariaAlimentacion = ((pesoPez - 261) / 5) * (1.9 - 1.93) + 1.93;
        }
        if (pesoPez >= 266 && pesoPez < 271)
        {
            tasaDiariaAlimentacion = ((pesoPez - 266) / 5) * (1.89 - 1.9) + 1.9;
        }
        if (pesoPez >= 271 && pesoPez < 276)
        {
            tasaDiariaAlimentacion = ((pesoPez - 271) / 5) * (1.88 - 1.89) + 1.89;
        }
        if (pesoPez >= 276 && pesoPez < 281)
        {
            tasaDiariaAlimentacion = ((pesoPez - 276) / 5) * (1.85 - 1.88) + 1.88;
        }
        if (pesoPez >= 281 && pesoPez < 286)
        {
            tasaDiariaAlimentacion = ((pesoPez - 281) / 5) * (1.84 - 1.85) + 1.85;
        }
        if (pesoPez >= 286 && pesoPez < 291)
        {
            tasaDiariaAlimentacion = ((pesoPez - 286) / 5) * (1.82 - 1.84) + 1.84;
        }
        if (pesoPez >= 291 && pesoPez < 296)
        {
            tasaDiariaAlimentacion = ((pesoPez - 291) / 5) * (1.81 - 1.82) + 1.82;
        }
        if (pesoPez >= 296 && pesoPez < 301)
        {
            tasaDiariaAlimentacion = ((pesoPez - 296) / 5) * (1.8 - 1.81) + 1.81;
        }

        //Peso entre 301 y 401 gramos
        if (pesoPez >= 301 && pesoPez < 306)
        {
            tasaDiariaAlimentacion = ((pesoPez - 301) / 5) * (1.79 - 1.8) + 1.8;
        }
        if (pesoPez >= 306 && pesoPez < 311)
        {
            tasaDiariaAlimentacion = ((pesoPez - 306) / 5) * (1.79 - 1.79) + 1.79;
        }
        if (pesoPez >= 311 && pesoPez < 316)
        {
            tasaDiariaAlimentacion = ((pesoPez - 311) / 5) * (1.78 - 1.79) + 1.79;
        }
        if (pesoPez >= 316 && pesoPez < 321)
        {
            tasaDiariaAlimentacion = ((pesoPez - 316) / 5) * (1.77 - 1.78) + 1.78;
        }
        if (pesoPez >= 321 && pesoPez < 326)
        {
            tasaDiariaAlimentacion = ((pesoPez - 321) / 5) * (1.77 - 1.77) + 1.77;
        }
        if (pesoPez >= 326 && pesoPez < 331)
        {
            tasaDiariaAlimentacion = ((pesoPez - 326) / 5) * (1.76 - 1.77) + 1.77;
        }
        if (pesoPez >= 331 && pesoPez < 336)
        {
            tasaDiariaAlimentacion = ((pesoPez - 331) / 5) * (1.74 - 1.76) + 1.76;
        }
        if (pesoPez >= 336 && pesoPez < 341)
        {
            tasaDiariaAlimentacion = ((pesoPez - 336) / 5) * (1.73 - 1.74) + 1.74;
        }
        if (pesoPez >= 341 && pesoPez < 346)
        {
            tasaDiariaAlimentacion = ((pesoPez - 341) / 5) * (1.72 - 1.73) + 1.73;
        }
        if (pesoPez >= 346 && pesoPez < 351)
        {
            tasaDiariaAlimentacion = ((pesoPez - 346) / 5) * (1.71 - 1.72) + 1.72;
        }
        if (pesoPez >= 351 && pesoPez < 356)
        {
            tasaDiariaAlimentacion = ((pesoPez - 351) / 5) * (1.7 - 1.71) + 1.71;
        }
        if (pesoPez >= 356 && pesoPez < 361)
        {
            tasaDiariaAlimentacion = ((pesoPez - 356) / 5) * (1.69 - 1.7) + 1.7;
        }
        if (pesoPez >= 361 && pesoPez < 366)
        {
            tasaDiariaAlimentacion = ((pesoPez - 361) / 5) * (1.68 - 1.69) + 1.69;
        }
        if (pesoPez >= 366 && pesoPez < 371)
        {
            tasaDiariaAlimentacion = ((pesoPez - 366) / 5) * (1.67 - 1.68) + 1.68;
        }
        if (pesoPez >= 371 && pesoPez < 376)
        {
            tasaDiariaAlimentacion = ((pesoPez - 371) / 5) * (1.65 - 1.67) + 1.67;
        }
        if (pesoPez >= 376 && pesoPez < 381)
        {
            tasaDiariaAlimentacion = ((pesoPez - 376) / 5) * (1.64 - 1.65) + 1.65;
        }
        if (pesoPez >= 381 && pesoPez < 386)
        {
            tasaDiariaAlimentacion = ((pesoPez - 381) / 5) * (1.63 - 1.64) + 1.64;
        }
        if (pesoPez >= 386 && pesoPez < 391)
        {
            tasaDiariaAlimentacion = ((pesoPez - 386) / 5) * (1.62 - 1.63) + 1.63;
        }
        if (pesoPez >= 391 && pesoPez < 396)
        {
            tasaDiariaAlimentacion = ((pesoPez - 391) / 5) * (1.61 - 1.62) + 1.62;
        }
        if (pesoPez >= 396 && pesoPez < 401)
        {
            tasaDiariaAlimentacion = ((pesoPez - 396) / 5) * (1.6 - 1.61) + 1.61;
        }

        //Peso entre 401 y 501 gramos
        if (pesoPez >= 401 && pesoPez < 406)
        {
            tasaDiariaAlimentacion = ((pesoPez - 401) / 5) * (1.59 - 1.6) + 1.6;
        }
        if (pesoPez >= 406 && pesoPez < 411)
        {
            tasaDiariaAlimentacion = ((pesoPez - 406) / 5) * (1.59 - 1.59) + 1.59;
        }
        if (pesoPez >= 411 && pesoPez < 416)
        {
            tasaDiariaAlimentacion = ((pesoPez - 411) / 5) * (1.58 - 1.59) + 1.59;
        }
        if (pesoPez >= 416 && pesoPez < 421)
        {
            tasaDiariaAlimentacion = ((pesoPez - 416) / 5) * (1.58 - 1.58) + 1.58;
        }
        if (pesoPez >= 421 && pesoPez < 426)
        {
            tasaDiariaAlimentacion = ((pesoPez - 421) / 5) * (1.57 - 1.58) + 1.58;
        }
        if (pesoPez >= 426 && pesoPez < 431)
        {
            tasaDiariaAlimentacion = ((pesoPez - 426) / 5) * (1.55 - 1.57) + 1.57;
        }
        if (pesoPez >= 431 && pesoPez < 436)
        {
            tasaDiariaAlimentacion = ((pesoPez - 431) / 5) * (1.55 - 1.55) + 1.55;
        }
        if (pesoPez >= 436 && pesoPez < 441)
        {
            tasaDiariaAlimentacion = ((pesoPez - 436) / 5) * (1.54 - 1.55) + 1.55;
        }
        if (pesoPez >= 441 && pesoPez < 446)
        {
            tasaDiariaAlimentacion = ((pesoPez - 441) / 5) * (1.53 - 1.54) + 1.54;
        }
        if (pesoPez >= 446 && pesoPez < 451)
        {
            tasaDiariaAlimentacion = ((pesoPez - 446) / 5) * (1.52 - 1.53) + 1.53;
        }
        if (pesoPez >= 451 && pesoPez < 456)
        {
            tasaDiariaAlimentacion = ((pesoPez - 451) / 5) * (1.51 - 1.52) + 1.52;
        }
        if (pesoPez >= 456 && pesoPez < 461)
        {
            tasaDiariaAlimentacion = ((pesoPez - 456) / 5) * (1.5 - 1.51) + 1.51;
        }
        if (pesoPez >= 461 && pesoPez < 466)
        {
            tasaDiariaAlimentacion = ((pesoPez - 461) / 5) * (1.49 - 1.5) + 1.5;
        }
        if (pesoPez >= 466 && pesoPez < 471)
        {
            tasaDiariaAlimentacion = ((pesoPez - 466) / 5) * (1.47 - 1.49) + 1.49;
        }
        if (pesoPez >= 471 && pesoPez < 476)
        {
            tasaDiariaAlimentacion = ((pesoPez - 471) / 5) * (1.46 - 1.47) + 1.47;
        }
        if (pesoPez >= 476 && pesoPez < 481)
        {
            tasaDiariaAlimentacion = ((pesoPez - 476) / 5) * (1.44 - 1.46) + 1.46;
        }
        if (pesoPez >= 481 && pesoPez < 486)
        {
            tasaDiariaAlimentacion = ((pesoPez - 481) / 5) * (1.43 - 1.44) + 1.44;
        }
        if (pesoPez >= 486 && pesoPez < 491)
        {
            tasaDiariaAlimentacion = ((pesoPez - 486) / 5) * (1.42 - 1.43) + 1.43;
        }
        if (pesoPez >= 491 && pesoPez < 496)
        {
            tasaDiariaAlimentacion = ((pesoPez - 491) / 5) * (1.41 - 1.42) + 1.42;
        }
        if (pesoPez >= 496 && pesoPez < 501)
        {
            tasaDiariaAlimentacion = ((pesoPez - 496) / 5) * (1.4 - 1.41) + 1.41;
        }

        //Peso entre 501 y 601 gramos
        if (pesoPez >= 501 && pesoPez < 506)
        {
            tasaDiariaAlimentacion = ((pesoPez - 501) / 5) * (1.39 - 1.4) + 1.4;
        }
        if (pesoPez >= 506 && pesoPez < 511)
        {
            tasaDiariaAlimentacion = ((pesoPez - 506) / 5) * (1.39 - 1.39) + 1.39;
        }
        if (pesoPez >= 511 && pesoPez < 516)
        {
            tasaDiariaAlimentacion = ((pesoPez - 511) / 5) * (1.39 - 1.39) + 1.39;
        }
        if (pesoPez >= 516 && pesoPez < 521)
        {
            tasaDiariaAlimentacion = ((pesoPez - 516) / 5) * (1.38 - 1.39) + 1.39;
        }
        if (pesoPez >= 521 && pesoPez < 526)
        {
            tasaDiariaAlimentacion = ((pesoPez - 521) / 5) * (1.37 - 1.38) + 1.38;
        }
        if (pesoPez >= 526 && pesoPez < 531)
        {
            tasaDiariaAlimentacion = ((pesoPez - 526) / 5) * (1.36 - 1.37) + 1.37;
        }
        if (pesoPez >= 531 && pesoPez < 536)
        {
            tasaDiariaAlimentacion = ((pesoPez - 531) / 5) * (1.36 - 1.36) + 1.36;
        }
        if (pesoPez >= 536 && pesoPez < 541)
        {
            tasaDiariaAlimentacion = ((pesoPez - 536) / 5) * (1.35 - 1.36) + 1.36;
        }
        if (pesoPez >= 541 && pesoPez < 546)
        {
            tasaDiariaAlimentacion = ((pesoPez - 541) / 5) * (1.35 - 1.35) + 1.35;
        }
        if (pesoPez >= 546 && pesoPez < 551)
        {
            tasaDiariaAlimentacion = ((pesoPez - 546) / 5) * (1.35 - 1.35) + 1.35;
        }
        if (pesoPez >= 551 && pesoPez < 556)
        {
            tasaDiariaAlimentacion = ((pesoPez - 551) / 5) * (1.34 - 1.35) + 1.35;
        }
        if (pesoPez >= 556 && pesoPez < 561)
        {
            tasaDiariaAlimentacion = ((pesoPez - 556) / 5) * (1.34 - 1.34) + 1.34;
        }
        if (pesoPez >= 561 && pesoPez < 566)
        {
            tasaDiariaAlimentacion = ((pesoPez - 561) / 5) * (1.34 - 1.34) + 1.34;
        }
        if (pesoPez >= 566 && pesoPez < 571)
        {
            tasaDiariaAlimentacion = ((pesoPez - 566) / 5) * (1.33 - 1.34) + 1.34;
        }
        if (pesoPez >= 571 && pesoPez < 576)
        {
            tasaDiariaAlimentacion = ((pesoPez - 571) / 5) * (1.32 - 1.33) + 1.33;
        }
        if (pesoPez >= 576 && pesoPez < 581)
        {
            tasaDiariaAlimentacion = ((pesoPez - 576) / 5) * (1.32 - 1.32) + 1.32;
        }
        if (pesoPez >= 581 && pesoPez < 586)
        {
            tasaDiariaAlimentacion = ((pesoPez - 581) / 5) * (1.32 - 1.32) + 1.32;
        }
        if (pesoPez >= 586 && pesoPez < 591)
        {
            tasaDiariaAlimentacion = ((pesoPez - 586) / 5) * (1.31 - 1.32) + 1.32;
        }
        if (pesoPez >= 591 && pesoPez < 596)
        {
            tasaDiariaAlimentacion = ((pesoPez - 591) / 5) * (1.31 - 1.31) + 1.31;
        }
        if (pesoPez >= 596 && pesoPez < 601)
        {
            tasaDiariaAlimentacion = ((pesoPez - 596) / 5) * (1.3 - 1.31) + 1.31;
        }

        //Peso mayor a 601 gramos
        if (pesoPez >= 601)
        {
            tasaDiariaAlimentacion = 1.3;
        }

        return tasaDiariaAlimentacion;
    }

}
