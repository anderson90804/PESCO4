using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuScript : MonoBehaviour
{
    public Button freeGameButton;
    public Button roomsButton;
    public Button creditsButton;
    public Button exitButton;

    // Start is called before the first frame update
    void Start()
    {
        freeGameButton.GetComponent<Button>().onClick.AddListener(() => {
            Debug.Log("Juego Libre");
        });
        roomsButton.GetComponent<Button>().onClick.AddListener(() => {
            SceneManager.LoadScene("levelMenu");
        });
        creditsButton.GetComponent<Button>().onClick.AddListener(() => {
            Debug.Log("Credits");
        });
        exitButton.GetComponent<Button>().onClick.AddListener(() => {
            Debug.Log("Exit");
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
