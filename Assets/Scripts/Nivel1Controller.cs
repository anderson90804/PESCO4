﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Nivel1Controller : MonoBehaviour {

    public bool juegoGanado = false;
    public GameLogicN1 datosjuego;
    public ModeloN1 modelo;
    //Declaración de variables de control para conocer si ha ganado el juego
    public double peso;
    public double factorConversion;
    public double racionRequerida;
    public double dinero;
    //Control de misiones
    public bool mision1completa;
    public bool mision2completa;
    public bool mision3completa;
    //Avisos de juego ganado o perdido
    public GameObject levelWin;
    public GameObject levelOver;
    public GameObject vendedor;

    void Start()
    {
        mision1completa = false;
        mision2completa = false;
        mision3completa = false;
        datosjuego.GetDineroDisponible();
        modelo.GetModeloActual();
        levelWin.gameObject.SetActive(false);
        levelOver.gameObject.SetActive(false);
    }

    private void Update()
    {
        this.dinero = datosjuego.GetDineroDisponible();
        if (modelo.GetProduccionActiva())
        {
            this.peso = (modelo.GetPesoPromedio());
            this.factorConversion = (modelo.GetFactorConversion());
            this.racionRequerida = modelo.GetRacionRequerida();
        }
        if ((int)this.dinero < 1)
        {
            GameOver();
        }
        //Validación misión 1
        if (((float)peso - 50f) > 0.01f)
        {
            mision1completa = true;
        }
        //Validación de misión 2
        if ((float)racionRequerida -2f > 0.01f)
        {
            mision2completa = true;
        }
        //Validación misión 3
        if ((float)dinero -1f > 0.01f)
        {
            mision3completa = true;
        }
    }

    public void ValidarJuego()
    {
        print("Peso de venta:"+(int)peso+" - Ración:"+(int)racionRequerida);
        if(mision1completa && mision2completa && mision3completa) {
            StartCoroutine(GameWin());
            GestionBD.singleton.RegistrarFinalizacionNivel();
        }
        else
        {
            GameOver();
        } 
        //StartCoroutine(GameWin());
    }

    IEnumerator GameWin()
    {
            Debug.Log("GANASTE----------------->");
            vendedor.GetComponent<Animator>().SetTrigger("venta");
            yield return new WaitForSeconds(3f);
            //GetComponent<AudioSource>().Play();
            GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>().enabled=false;
            levelWin.gameObject.SetActive(true);
            juegoGanado = true;
    }

    private void GameOver()
    {
            GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>().enabled = false;
            levelOver.gameObject.SetActive(true);
    }

}
